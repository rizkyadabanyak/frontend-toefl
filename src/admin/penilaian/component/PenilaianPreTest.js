import React, { Component } from 'react'
import { useNavigate } from 'react-router-dom';
import Sidebar from "../component/Sidebar";
import { Alert, Button, notification, Spin, Input, Space, Table, TimePicker } from 'antd';

function PenilaianPreTest() {
    let navigate = useNavigate();

    const columns = [
        {
            title: 'Sesi',
            dataIndex: 'sesi',
            key: 'sesi',
            width: '30%',
            className: 'text-center',
            // ...getColumnSearchProps('sesi'),
        },
        {
            title: 'Skor Benar',
            dataIndex: 'benar',
            key: 'benar',
            width: '30%',
            className: 'text-center',
            // ...getColumnSearchProps('benar'),
        },
        {
            title: 'Skor Salah',
            dataIndex: 'salah',
            key: 'salah',
            width: '30%',
            className: 'text-center',
            // ...getColumnSearchProps('salah'),
        },
        {
            title: 'Perhitungan',
            dataIndex: 'perhitungan',
            key: 'perhitungan',
            width: '30%',
            className: 'text-center',
            // ...getColumnSearchProps('perhitungan'),
        },
        {
            // title: 'Perhitungan',
            dataIndex: 'edit',
            key: 'edit',
            width: '30%',
            className: 'text-center',
            // ...getColumnSearchProps('perhitungan'),
        },
    ];

    return (
        <div className="container-fluid page-body-wrapper">
            <Sidebar />
            <div className="main-panel">
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">

                                        <div className="col-lg-12 grid-margin stretch-card">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h4 className="card-title">Penilaian Pre-Test</h4>
                                                    <div className="table-responsive pt-3">
                                                        <Table pagination={{ pageSize: 5 }}
                                                            columns={columns}></Table>
                                                    </div>
                                                    {/* <Table pagination={{ pageSize: 5 }}
                                                            loading={loading ? true : false} columns={columns} dataSource={data}></Table> */}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer className="footer">
                    <div className="d-sm-flex justify-content-center justify-content-sm-between">
                        <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
                        <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                    </div>
                </footer>
            </div>
        </div>

    )
}
export default PenilaianPreTest;
