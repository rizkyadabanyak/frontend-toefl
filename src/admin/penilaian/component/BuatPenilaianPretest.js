import React, { Component, useEffect, useRef, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import Sidebar from "../component/Sidebar";
import { Alert, Button, notification, Spin, message, Modal, Steps, Input, Space, Table, TimePicker, Form, Pagination } from 'antd';
import moment from 'moment';
import axios from 'axios';
import Url from "../../../config"
import Swal from 'sweetalert2';
import Highlighter from 'react-highlight-words';
import { DeleteOutlined, EditOutlined, LoadingOutlined, PlusOutlined, SearchOutlined } from "@ant-design/icons";
import "../style/style.css"
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ReactHTMLParser from 'html-react-parser';

function BuatPenilaianPretest() {
    // soal
    const [satuanSoal, setSatuanSoal] = useState(); 
    // onchange input soal
    const handleChange = (e, editor) => {
        const data = editor.getData();
        setSatuanSoal(data);
    }
    
    const [dataSoal, setDataSoal] = useState([]);
    const [modalBuatNomor, setModalBuatNomor] = useState(false);
    const [listening, setListening] = useState(false);
    const [reading, setReading] = useState(false);
    // long question
    const [questionLong, setQuestionLog] = useState();
    // onchange input longquestion
    const handleChangeQuestion = (e, editor) => {
        const data = editor.getData();
        setQuestionLog(data);
    }
    const [dataSpaces, setDataSpaces] = useState([]);
    const [minIndex, setMinIndex] = useState(0);
    const [maxIndex, setMaxIndex] = useState(0);
    const [idxSpace, setIdxSpace] = useState(0);
    const [loading, setLoading] = useState(true);
    const [current, setCurrent] = useState();
    const [pilihanSection, setPilihanSection] = useState("reading")

    const pageSize = 1;

    // query 
    const { search } = useLocation();
    const query = new URLSearchParams(search);
    const id = query.get("id");
    const searchInput = useRef(null);

    // token 
    let token = sessionStorage.getItem("token");

    // reading 
    const [showModalReading, setShowModalReading] = useState(false);
    const [itemReading, setItemReading] = useState();

    const [idKirim, setIdKirim] = useState();
    const [idSpaces, setIdSpaces] = useState();
    const [showModalSoal, setShowModalSoal] = useState(false);
    // const [question, setQuestion] = useState();
    const [jumlahJawaban, setJumlahJawaban] = useState([]);
    const [kunciJawaban, setKunciJawaban] = useState('A');
    const [question, setQuestion] = useState([]);
    const [answer0, setAnswer0] = useState();
    const [answer1, setAnswer1] = useState();
    const [answer2, setAnswer2] = useState();
    const [answer3, setAnswer3] = useState();
    const [alphabet, setAlphabet] = useState(['A', 'B', 'C', 'D'])

    useEffect(() => {
        const task = async () => {
            await getDataSpaces();
            await getDataQuestion();
        }
        task();
    }, []);

    const getDataSpaces = async () => {
        const formData = new FormData();
        formData.append('token', token);
        formData.append('category_id', id);

        await axios.post(`${Url}/api/v1/admin/operate/space/show`, formData)
            .then((res) => {
                console.log("yaaa");
                setDataSoal(res.data.data[0]);
                setDataSpaces(res.data.data[0].spaces);
                let space = res.data.data[0].spaces;
                let tmpSpace = [];

                if (space.length != 0) {
                    for (let i = 0; i < space.length; i++) {
                        tmpSpace.push(space[i].id);
                    }
                }

                setIdSpaces(tmpSpace);
                getDataQuestion(space[0].id);
                // console.log(res.data.index_space);
            })
    }

    const getDataQuestion = async (idx) => {
        console.log(idx)
        console.log(idSpaces[idx])
        const formData = new FormData();
        formData.append('token', token);
        formData.append('category_id', id);
        formData.append('number_space', idSpaces[idx]);


        await axios.post(`${Url}/api/v1/admin/operate/question/answer/show`, formData)
            .then((res) => {
                setQuestion(res.data.data[0].questions);
            }).then(() => {
                // changePagination(1)
                setLoading(false);
            })
    }

    function klikBuatNomor() {
        const formData = new FormData();

        formData.append('category_id', id);
        formData.append('long_question', questionLong);
        formData.append('section', pilihanSection);
        formData.append('token', token);

        axios.post(`${Url}/api/v1/admin/operate/space/create`, formData)
            .then(res => {
                if (res.data.status == 'success') {
                    Swal.fire(
                        'Selamat!',
                        res.data.message,
                        'success'
                    )
                }
                else {
                    Swal.fire(
                        'Oopsss...',
                        res.data.message,
                        'error'
                    )

                }
                getDataSpaces();
                setModalBuatNomor(false)

                // window.location.reload()

            })


    }

    function klikTampilPertanyaan(value) {
        console.log(value)
        if (value == "Y" && pilihanSection == "reading") {
            setReading(true);
            // setListening(false);
        }
        else {
            // setListening(false);
            setReading(false);
        }
    }

    function klikPilihSection(value) {
        setPilihanSection(value);

        let selected = document.getElementById('yorn').value
        if (selected == "Y" && value == "reading") {
            setReading(true);
        }
        else {
            // setListening(false);
            setReading(false);
        }
    }

    function klikTambahSoal(id) {
        console.log(id);
        setIdKirim(id);
        setShowModalSoal(true);
    }

    function klikSimpanSoal() {
        // let tmp = [];
        let answer = [];
        answer.push(answer0);
        answer.push(answer1);
        answer.push(answer2);
        answer.push(answer3);
        const formData = new FormData();

        formData.append('question', satuanSoal);
        formData.append('space_id', idKirim);
        formData.append('token', token);

        axios.post(`${Url}/api/v1/admin/operate/question/create`, formData)
            .then((res) => {
                if (res.data.status == 'success') {
                    const idQuestion = res.data.data.id;

                    for (let i = 0; i < alphabet.length; i++) {
                        const formJawaban = new FormData();
                        formJawaban.append('question_id', idQuestion);
                        formJawaban.append('token', token);
                        formJawaban.append('alphabet', alphabet[i]);
                        formJawaban.append('answer', answer[i]);
                        axios.post(`${Url}/api/v1/admin/operate/question/answer/create`, formJawaban);
                    }

                    const formKunciJawaban = new FormData();
                    formKunciJawaban.append('question_id', idQuestion);
                    formKunciJawaban.append('token', token);
                    formKunciJawaban.append('alphabet_key', kunciJawaban);
                    axios.post(`${Url}/api/v1/admin/operate/question/answer/create/key`, formKunciJawaban)
                        .then(res2 => {
                            if (res2.data.status == 'success') {
                                Swal.fire(
                                    'Selamat!',
                                    res.data.message,
                                    'success',
                                )
                                // window.location.reload()
                                getDataQuestion(idxSpace);
                                setShowModalSoal(false);

                            }
                        })


                }

            })

    }

    function changePagination(page) {
        setIdxSpace(page - 1)
        setCurrent(page);
        setMinIndex((page - 1) * pageSize);
        setMaxIndex(page * pageSize);
        getDataQuestion(page - 1);
    }


    return (
        <div className="container-fluid page-body-wrapper">
            <Sidebar />
            <div className="main-panel">
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="col-lg-12 grid-margin stretch-card">
                                            <div className="card">
                                                <div className="card-body">
                                                    <div className='d-flex justify-content-between'>
                                                        <h4 className="card-title">List Soal Pre-Test</h4>
                                                    </div>
                                                    {/* <div className="table-responsive d-flex flex-column pt-3" style={{ height: "380px", display: loading ? "flex" : "none" }}></div> */}
                                                    <div className="table-responsive d-flex flex-column pt-3" style={{ height: "380px", display: loading ? "none" : "flex" }}>
                                                        {
                                                            dataSpaces.length == 0 ?
                                                                <div className='d-flex flex-column justify-content-center align-items-center' style={{ height: "90%", width: "100%" }}>
                                                                    <Button type="primary" style={{ width: "50px" }}>
                                                                        <PlusOutlined style={{ verticalAlign: "0em" }} onClick={() => setModalBuatNomor(true)} />
                                                                    </Button>
                                                                    <div className='mt-2'>Belum ada space, tambahkan sekarang</div>
                                                                </div>
                                                                :
                                                                idxSpace == dataSpaces.length ?
                                                                    <div className='d-flex flex-column justify-content-center align-items-center' style={{ height: "90%", width: "100%" }}>
                                                                        <Button type="primary" style={{ width: "50px" }}>
                                                                            <PlusOutlined style={{ verticalAlign: "0em" }} onClick={() => setModalBuatNomor(true)} />
                                                                        </Button>
                                                                        <div className='mt-2'>Tambahkan space baru</div>
                                                                    </div>
                                                                    :
                                                                    <>
                                                                        {

                                                                            dataSpaces?.map((item, i) =>
                                                                                i >= minIndex &&
                                                                                i < maxIndex && (
                                                                                    <>
                                                                                        {item.section == "listening" ?
                                                                                            <div className='long-question'><audio controls><source src={Url + "/" + item.long_question} type="audio/mpeg" /></audio></div> :
                                                                                            item.section == "reading" ?
                                                                                                <div className='long-question'>{item.long_question ? ReactHTMLParser(item.long_question) : ''}</div> : null
                                                                                        }
                                                                                        {
                                                                                            question.length == 0 ?
                                                                                                <div className='d-flex flex-column justify-content-center align-items-center' style={{ height: "70%", width: "100%" }}>
                                                                                                    <Button type="primary" className='me-4' primary onClick={() => klikTambahSoal(item.id)}>Tambahkan Soal</Button>
                                                                                                </div>
                                                                                                :
                                                                                                <>{
                                                                                                    question?.map((soal, x) =>
                                                                                                    (
                                                                                                        <>
                                                                                                            <div className='d-flex pertanyaan'>
                                                                                                                <div className='no-soal'>{x + 1}</div>
                                                                                                                <div className='soal'>{soal.question ? ReactHTMLParser(soal.question) : ''}</div>
                                                                                                            </div>
                                                                                                            {
                                                                                                                soal.answer.length == 0 ? <div>Belum setting Jawaban </div> :
                                                                                                                    <>
                                                                                                                        {

                                                                                                                            <>
                                                                                                                                <div className='jawaban'>
                                                                                                                                    {
                                                                                                                                        soal.answer?.map((data, y) => (
                                                                                                                                            <div>{data.alphabet}. {data.answer}</div>

                                                                                                                                        )
                                                                                                                                        )
                                                                                                                                    }
                                                                                                                                </div>
                                                                                                                                <div className='kunci-jawaban'>Kunci Jawaban : {soal.key[0].alphabet_key}</div>
                                                                                                                            </>
                                                                                                                        }

                                                                                                                    </>
                                                                                                            }
                                                                                                        </>
                                                                                                    ))}

                                                                                                    < div className='d-flex flex-column justify-content-center align-items-center mt-4 mb-4' style={{ height: "70%", width: "100%" }}>
                                                                                                        <Button type="primary" className='me-4' primary onClick={() => klikTambahSoal(item.id)}>Tambahkan Soal</Button>
                                                                                                    </div>
                                                                                                </>
                                                                                        }

                                                                                    </>

                                                                                )
                                                                            )
                                                                        }
                                                                    </>

                                                        }
                                                        <div className='d-flex justify-content-end pe-3'>
                                                            <Pagination
                                                                pageSize={pageSize}
                                                                current={current}
                                                                total={dataSpaces.length + 1}
                                                                onChange={changePagination}
                                                                style={{ bottom: "0px" }}
                                                            />
                                                        </div>
                                                        {/* <Table
                                                            loading={dataSoal.length == 0 ? true : dataSpaces.length == 0 ? false : false}
                                                            columns={columns}
                                                            dataSource={dataSpaces}
                                                            expandable={{
                                                                expandedRowRender: (record, index) => expandedRowRender(record, index)
                                                                // rowExpandable: (record) => record.question.key !== 'undefined',
                                                            }}
                                                        /> */}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    title="Tambahkan Soal"
                    centered
                    visible={modalBuatNomor}
                    onOk={() => klikBuatNomor()}
                    onCancel={() => setModalBuatNomor(false)}
                >
                    <div class="form-group">
                        <label>Pilih Section</label>
                        <select class="form-select" onClick={(e) => klikPilihSection(e.target.value)}>
                            {/* <option value="listening">Listening</option> */}
                            <option value="reading">Reading</option>
                            <option value="reading">Structure</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Satu Topik untuk Banyak Soal?</label>
                        <select class="form-select" id='yorn' onClick={(e) => klikTampilPertanyaan(e.target.value)}>
                            <option value="T">Tidak</option>
                            <option value="Y">Ya</option>
                        </select>
                    </div>
                    <div className="form-group" style={{ display: reading ? "block" : "none" }}>
                        <label htmlFor="exampleInputEmail3">Masukkan Bacaan</label>
                        {/* <textarea className="form-control ckeditor" name="content" id="exampleInputEmail3" style={{ height: "100px" }} onChange={(e) => setQuestionLog(e.target.value)} ></textarea> */}
                        <div>
                            <CKEditor
                                editor={ClassicEditor}
                                data={questionLong}
                                onChange={handleChangeQuestion}
                            />
                        </div>
                    </div>
                    <div className="form-group" style={{ display: listening ? "block" : "none" }}>
                        <label htmlFor="exampleInputEmail3">Upload Audio</label>
                        <input type="file" className="form-control" id="exampleInputEmail3" placeholder="Masukkan Soal" onChange={(e) => setQuestionLog(e.target.files[0])} />
                    </div>
                </Modal>

                <Modal
                    title="Soal Reading"
                    centered
                    visible={showModalReading}
                    onOk={() => setShowModalReading(false)}
                    onCancel={() => setShowModalReading(false)}
                >
                    {itemReading}
                </Modal>

                <Modal
                    title="Tambah Soal"
                    centered
                    visible={showModalSoal}
                    onOk={() => klikSimpanSoal()}
                    onCancel={() => setShowModalSoal(false)}
                >
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail3">Soal</label>
                        {/* <input type="textarea" className="form-control" id="exampleInputEmail3" onChange={(e) => setSatuanSoal(e.target.value)} /> */}
                        <CKEditor
                            editor={ClassicEditor}
                            data={satuanSoal}
                            onChange={handleChange}
                        />
                    </div>
                    <div className="form-group ">
                        <label htmlFor="exampleInputEmail3">Opsi Jawaban</label>
                        {/* <><Button type="primary" className='ms-4 mb-4' size="small" onClick={()=> setJumlahJawaban(...jumlahJawaban, [])}><PlusOutlined style={{ verticalAlign: "0em" }} /></Button></> */}
                        {/* {
                            jumlahJawaban.map((item, i) => { */}
                        <Input size="large" placeholder="Masukkan jawaban" prefix="A." className='mt-2' onChange={(e) => setAnswer0(e.target.value)} />
                        <Input size="large" placeholder="Masukkan jawaban" prefix="B." className='mt-2' onChange={(e) => setAnswer1(e.target.value)} />
                        <Input size="large" placeholder="Masukkan jawaban" prefix="C." className='mt-2' onChange={(e) => setAnswer2(e.target.value)} />
                        <Input size="large" placeholder="Masukkan jawaban" prefix="D." className='mt-2' onChange={(e) => setAnswer3(e.target.value)} />
                        {/* })
                        } */}
                        {/* <input type="textarea" className="form-control" id="exampleInputEmail3" /> */}
                    </div>
                    <div class="form-group">
                        <label>Kunci Jawaban</label>
                        <select class="form-select" onClick={(e) => setKunciJawaban(e.target.value)}>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                        </select>
                    </div>

                </Modal>

                <footer className="footer">
                    <div className="d-sm-flex justify-content-center justify-content-sm-between">
                        <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
                        <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                    </div>
                </footer>
            </div>
        </div >

    )
}
export default BuatPenilaianPretest;
