import Login from "../../login/component/Login";
import Navbar from "../../../user/landingpage/component/Navbar";
// import "../style/Landingpage.css";

function ViewLogin() {
  return (
    <div className="viewlogin">
      <Navbar/>
      <Login/>
    </div>
  );
}

export default ViewLogin;
