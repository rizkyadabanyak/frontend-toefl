import Sidebar from "../component/Sidebar";
import { DeleteOutlined, EditOutlined, ForwardOutlined, LoadingOutlined, SearchOutlined } from "@ant-design/icons";
import 'antd/dist/antd.css';
// import '../style/DataUser.css'
import { UserOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';
import Highlighter from 'react-highlight-words';
import React, { Component, useEffect, useRef, useState } from 'react'
import moment from 'moment';
import axios from 'axios';
import Url from "../../../config";
// import "../../../style/SoalPreTest.css";
import audiotes from "../../../asset/005.mp3";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
// import { useNavigate } from 'react-router-dom';
import Countdown, { formatTimeDelta, zeroPad } from 'react-countdown';
// import type { RadioChangeEvent } from 'antd';
import { RadioChangeEvent, Button, Pagination, Radio, Space, Input, Spin, Tag, Table } from 'antd';
import Swal from "sweetalert2";
import ReactHTMLParser from 'html-react-parser';
import {
    CheckCircleOutlined,
    CloseCircleOutlined,
} from '@ant-design/icons';
const format = 'HH:mm';


const Completionist = () => <span>You are good to go!</span>;



function DetailHistory() {
    // const [dataAnswer, setAnswer] = useState([]);
    const [loading, setLoading] = useState(true);
    const [loadSoal, setLoadSoal] = useState(true);
    // const [idPaket, setIdPaket] = useState();
    const navigate = useNavigate()
    // const [namaPaket, setNamaPaket] = useState();
    // const [durasiWaktu, setDurasiWaktu] = useState("00:00");
    // const [dataPaket, setDataPaket] = useState([]);
    // const [loading, setLoading] = useState(true);
    // const [dataNamaPaketId, setDataNamaPaketId] = useState();
    // const [dataWaktuId, setDataWaktuId] = useState();
    // const [dataId, setDataId] = useState();
    // const [searchText, setSearchText] = useState('');
    // const [searchedColumn, setSearchedColumn] = useState('');
    const [spaceNow, setSpaceNow] = useState(0)
    const [indexSpace, setIndexSpace] = useState([]);
    const [longQuestion, setLongQuestion] = useState();
    const [pembahasan, setPembahasan] = useState();
    const [countQuestion, setCountQuestion] = useState([]);
    const [kunciJawaban, setKunciJawaban] = useState([]);
    const [kategori, setKategori] = useState();
    const [section, setSection] = useState('');
    const [jawabanUser, setJawabanUser] = useState([]);
    // const searchInput = useRef(null);
    // const [modalEditExam, setModalEditExam] = useState(false);

    const { search } = useLocation();
    // const query = new URLSearchParams(search);
    const session = sessionStorage.getItem('session')

    // pengambilan data awal. Mencari space dan jumlah pertanyaan
    useEffect(() => {
        const formData = new FormData();
        formData.append('session', session);

        axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, formData)
            .then((res) => {
                setCountQuestion(res.data.countQuestionBySpace)
                setIndexSpace(res.data.index_space);
                console.log(res.data.index_space)
                setKategori(res.data.data.info_exam.categories.categories)

            })
    }, []);


    // pengambilan data space pertama 
    useEffect(() => {
        console.log(indexSpace)
        let spaceOne = new FormData();

        spaceOne.append('session', session);
        spaceOne.append('number_space', indexSpace[0]);

        axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, spaceOne)
            .then(response => {
                console.log(response.data.data)
                setLongQuestion(response.data.data.question[0].long_question)
                setPembahasan(response.data.data.question[0].questions);
                let pembahasan = response.data.data.question[0].questions
                let section = response.data.data.question[0].section;
                if (section == 'reading') {
                    setSection('Reading')
                }
                else if (section == 'listening') {
                    setSection('Listening')
                }
                else if (section == 'structure') {
                    setSection('Structure')
                }

                let arrJawaban = [];
                let arrKunci = [];
                // menampung jawaban dan pertanyaan semua space 
                for (let x = 0; x < indexSpace.length; x++) {
                    let tmpJawaban = [];
                    let tmpKunci = []

                    if (x == 0) {
                        for (let i = 0; i < countQuestion[x]; i++) {
                            tmpJawaban.push(pembahasan[i].he_answer.length > 0 ? pembahasan[i].he_answer[0].he_answer : '')
                            tmpKunci.push(pembahasan[i].key[0].alphabet_key)
                        }
                    }
                    else {
                        for (let i = 0; i < countQuestion[x]; i++) {
                            tmpJawaban.push("")
                            tmpKunci.push("")
                        }
                    }

                    arrJawaban.push(tmpJawaban);
                    arrKunci.push(tmpKunci);
                }

                setJawabanUser(arrJawaban);
                setKunciJawaban(arrKunci);

            }).then(() => {
                setLoading(false)
                setLoadSoal(false)

            })
    }, [indexSpace])

    const getNextBackData = (index) => {
        let spaceOne = new FormData();
        let section;

        spaceOne.append('number_space', indexSpace[index]);
        spaceOne.append('session', session);
        axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, spaceOne)
            .then(response => {
                if (response.data.data.question.length > 0) {
                    setLongQuestion(response.data.data.question[0].long_question)
                    let pembahasan = response.data.data.question[0].questions
                    setPembahasan(response.data.data.question[0].questions);

                    section = response.data.data.question[0].section;
                    if (section == 'reading') {
                        setSection('Reading')
                    }
                    else if (section == 'listening') {
                        setSection('Listening')
                    }
                    else if (section == 'structure') {
                        setSection('Structure')
                    }

                    let arrJawaban = [];
                    let arrKunci = [];
                    for (let x = 0; x < indexSpace.length; x++) {
                        let tmpJawaban = [];
                        let tmpKunci = []

                        if (x == index) {
                            for (let i = 0; i < countQuestion[x]; i++) {
                                tmpJawaban.push(pembahasan[i].he_answer.length > 0 ? pembahasan[i].he_answer[0].he_answer : '')
                                tmpKunci.push(pembahasan[i].key[0].alphabet_key)
                            }
                        }
                        else {
                            for (let i = 0; i < countQuestion[x]; i++) {
                                tmpJawaban.push(jawabanUser[x][i])
                                tmpKunci.push(kunciJawaban[x][i])
                            }
                        }

                        arrJawaban.push(tmpJawaban);
                        arrKunci.push(tmpKunci);

                    }

                    setJawabanUser(arrJawaban);
                    setKunciJawaban(arrKunci);
                }

            }).then(() => setLoadSoal(false))

    }

    function klikBack(idxSekarang) {
        setLongQuestion('')
        setPembahasan([])
        setLoadSoal(true)
        getNextBackData(idxSekarang - 1);
        setSpaceNow(spaceNow - 1);
    }

    function klikNext(idxSekarang) {
        setLongQuestion('')
        setPembahasan([])
        setLoadSoal(true)
        getNextBackData(idxSekarang + 1);
        setSpaceNow(idxSekarang + 1);
    }

    if (loading) {

        return <div className="container-fluid page-body-wrapper">
            <Sidebar />
            <Spin className='loading' tip="Loading..." size="large" />
        </div>
    }

    return (
        <div className="container-fluid page-body-wrapper">
            <Sidebar />

            {/* <Spin className='loading' tip="Loading..." size="large" style={{ display: loading ? "blok" : "none" }} /> */}

            <div className="main-panel">

                <div className="soalpretest">
                    <div className="d-flex col flex-lg-row flex-column-reverse mt-5">
                        <div className="col-xl-7 col-lg-12 me-auto ms-auto">
                            <div class="card judul-pretest mb-5">
                                <div class="card-header">
                                    <div className="d-flex justify-content-between">
                                        <div className="col mt-0">{kategori}</div>
                                        <div className="col mt-0">Section {spaceNow + 1} / {indexSpace.length} {section}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="card soal-pretest p-4" >
                                <p class="card-text mb-2">{ReactHTMLParser(longQuestion)}</p>
                                <div class="card-body">
                                    <div className="justify-content-center mb-4 mt-2" style={{ display: loadSoal ? "flex" : "none" }}>
                                        <Spin tip="Loading...">
                                        </Spin>
                                    </div>
                                    {
                                        pembahasan.map((item, i) => (
                                            <>


                                                <div className="d-flex mb-2 mt-2">

                                                    <p class="card-text me-2">{i + 1}.</p>
                                                    <p class="card-text">{ReactHTMLParser(item.question)}</p>
                                                </div>


                                                {

                                                    <>
                                                        <Radio.Group disabled key={spaceNow + i} className={"pilihanke" + spaceNow + i} defaultValue={jawabanUser[spaceNow][i]}  >
                                                            <Space direction="vertical"  >
                                                                {
                                                                    item.answer.map((answer, x) => (
                                                                        <Radio value={answer.alphabet}>
                                                                            {answer.alphabet}. {answer.answer}
                                                                        </Radio>
                                                                    ))
                                                                }

                                                            </Space>
                                                        </Radio.Group>

                                                        <div className="d-flex">
                                                            {

                                                                jawabanUser[spaceNow][i] != kunciJawaban[spaceNow][i] ?

                                                                    <Tag style={{ verticalAlign: "none" }} className="mt-2" icon={<CloseCircleOutlined />} color="error">
                                                                        Key : {kunciJawaban[spaceNow][i]}
                                                                    </Tag>

                                                                    :

                                                                    <Tag style={{ verticalAlign: "none" }} className="mt-2" icon={<CheckCircleOutlined />} color="success">
                                                                        Key: {kunciJawaban[spaceNow][i]}
                                                                    </Tag>


                                                            }

                                                        </div>
                                                    </>
                                                }



                                            </>


                                        ))
                                    }
                                    <div className="d-flex justify-content-end mt-5">
                                        {
                                            spaceNow == 0 ?
                                                <button type="button" class="btn btn-primary button-back-soal me-4 text-white" onClick={() => klikBack(spaceNow)} disabled>Back</button> :
                                                <button type="button" class="btn btn-primary button-back-soal me-4" onClick={() => klikBack(spaceNow)}>Back</button>
                                        }
                                        {
                                            spaceNow == indexSpace.length - 1 ?
                                                <button type="button" class="btn btn-primary button-next-soal me-4" disabled onClick={() => klikNext(spaceNow)}>Next</button> :
                                                <button type="button" class="btn btn-primary button-next-soal me-4" onClick={() => klikNext(spaceNow)}>Next</button>
                                        }
                                        <button class="btn btn-primary button-finish-soal" type="button"
                                            onClick={() => {
                                                sessionStorage.removeItem("session"); navigate("/history")
                                            }}
                                        >Selesai</button>

                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </div >
            </div>
        </div>


    )
}

export default DetailHistory;
