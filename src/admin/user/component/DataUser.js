import React, { Component, useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Sidebar from "../component/Sidebar";
import axios from 'axios';
import { DeleteOutlined, EditOutlined, LoadingOutlined, SearchOutlined } from "@ant-design/icons";
import Swal from 'sweetalert2';
import 'antd/dist/antd.css';
import '../style/DataUser.css'
import { UserOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';
import { Button, Input, Space, Table, Spin, Tag } from 'antd';
import Highlighter from 'react-highlight-words';
import Url from "../../../config";

function DataUser() {
    let navigate = useNavigate();
    let token = sessionStorage.getItem("token");
    const [dataMurid, setDataMurid] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const searchInput = useRef(null);

    useEffect(() => {

        // setLoadKode(true);
        let formData = new FormData();

        formData.append('token', token);
        axios.post(`${Url}/api/v1/admin/operate/student-all`, formData)
            .then(res => {
                if (res.data.status == 'success') {
                    setDataMurid(res.data.data);
                }
                else {
                    Swal.fire(
                        'Gagal!',
                        res.data.message,
                        'error'
                    )
                }
                setLoading(false);

            });
    }, [])



    const data =
        [...dataMurid.map((item, i) => ({
            key: i + 1,
            img:
                <>{
                    item.img == "default" ?
                        <Avatar size={34} icon={<UserOutlined />} />
                        :
                        <img src={item.img} alt="image" />
                }</>,
            name: item.name,
            email: item.email,
            action: 
            <div className="d-flex justify-content-center">
            {/* <Button type="primary" className='me-4' danger><DeleteOutlined /></Button>
            
            <Button type="primary" success><EditOutlined /></Button> */}
            </div>

        }))
        ];

    // tabel 
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() => clearFilters && handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: '#ffc069',
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    // tabel 

    const columns = [
        {
            title: 'No.',
            dataIndex: 'key',
            key: 'key',
            width: '10%',
            className: 'text-center',
            ...getColumnSearchProps('key'),
        },{
            title: 'Foto Profile',
            dataIndex: 'img',
            key: 'img',
            width: '20%',
            className: 'text-center',
            ...getColumnSearchProps('img'),
        },
        {
            title: 'Nama',
            dataIndex: 'name',
            key: 'name',
            width: '20%',
            className: 'text-center',
            ...getColumnSearchProps('name'),
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            width: '25%',
            className: 'text-center',
            ...getColumnSearchProps('email'),
        },
        // {
        //     title: 'Action',
        //     dataIndex: 'action',
        //     key: 'action',
        //     width: '25%',
        //     className: 'text-center',
        //     ...getColumnSearchProps('action'),
        // },
    ];



    return (
        <div className="container-fluid page-body-wrapper">
            {/* partial:partials/_settings-panel.html */}
            {/* partial */}
            {/* partial:partials/_sidebar.html */}
            <Sidebar />

            <Spin className='loading' tip="Loading..." size="large" style={{ display: loading ? "blok" : "none" }} />

            <div className="main-panel" style={{ display: loading ? "none" : "flex" }}>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-lg-12 d-flex flex-column">

                                                <div className="col-lg-12 grid-margin stretch-card">
                                                    <div className="card">
                                                        <div className="card-body">
                                                            <h4 className="card-title">Data User</h4>
                                                            <div className="table-responsive">
                                                                <Table  columns={columns} dataSource={data} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* content-wrapper ends */}
                {/* partial:partials/_footer.html */}
                <footer className="footer">
                    <div className="d-sm-flex justify-content-center justify-content-sm-between">
                        <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
                        <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                    </div>
                </footer>
                {/* partial */}
            </div>
            {/* main-panel ends */}
        </div>

    )
}
export default DataUser;
