// import React, { Component } from 'react'
// import { useNavigate } from 'react-router-dom';
// import Sidebar from "../../dasboard/component/Sidebar";
import React, { Component, useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Sidebar from "../component/Sidebar";
import { Alert, Button, notification, Spin, Input, Space, Table, TimePicker, Modal } from 'antd';
import moment from 'moment';
import axios from 'axios';
import Url from "../../../config"
import Swal from 'sweetalert2';
import Highlighter from 'react-highlight-words';
import { DeleteOutlined, EditOutlined, LoadingOutlined, PlusOutlined, SearchOutlined } from "@ant-design/icons";
import "../style/style.css"
const format = 'HH:mm';

function SoalPreTestAdmin() {
    let navigate = useNavigate();
    const [idPaket, setIdPaket] = useState();
    const [namaPaket, setNamaPaket] = useState();
    const [durasiWaktu, setDurasiWaktu] = useState("00:00");
    const [dataPaket, setDataPaket] = useState([]);
    const [loading, setLoading] = useState(true);
    const [dataNamaPaketId, setDataNamaPaketId] = useState();
    const [dataWaktuId, setDataWaktuId] = useState();
    const [dataId, setDataId] = useState();
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const searchInput = useRef(null);
    const [modalEditExam, setModalEditExam] = useState(false);
    let token = sessionStorage.getItem("token");

    useEffect(() => {
        getDataPaket()
    }, []);

    function getDataPaket() {
        setLoading(true);
        const formData = new FormData();
        formData.append('token', token);
        formData.append('type', 'pre-test');

        axios.post(`${Url}/api/v1/admin/operate/question/show/exam`, formData)
            .then((res) => {
                setDataPaket(res.data.data);
            }).then(setLoading(false))
    }

    function editExam(id) {
        setIdPaket(id);
        // setLoading(true);
        const formData = new FormData();
        formData.append('id', id);
        formData.append('token', token);
        formData.append('type', 'pre-test');

        axios.post(`${Url}/api/v1/admin/operate/show/detail/exam`, formData)
            .then((res) => {
                console.log(res.data.data.time);
                // let namapaket = res.data.data.name;
                setDataNamaPaketId(res.data.data.name.toString());
                // let waktu = res.data.data.time;
                setDataWaktuId(res.data.data.time.toString());
                setDataId(res.data.data);
                // setDataPaket(res.data.data);
            }).then(setModalEditExam(true))
    }

    function deleteExam(id) {
        setIdPaket(id);
        const formData = new FormData();
        formData.append('id', id);
        formData.append('token', token);
        formData.append('type', 'pre-test');

        axios.post(`${Url}/api/v1/admin/operate/question/destroy/exam`, formData)
            .then((res) => {
                getDataPaket()
                // window.location.reload();
            }).then()
    }

    function klikSimpanPaket(value) {

        if (durasiWaktu === "00:00") {
            // console.log("gagal");
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Durasi Wajib Diisi',
            })
        }
        else {

            // console.log(value);
            const formData = new FormData();

            var a = durasiWaktu.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            var minute = (+a[0]) * 60 + (+a[1]);
            console.log(minute);


            formData.append('name', namaPaket);
            formData.append('time', minute);
            formData.append('option', 'pre-test');
            formData.append('token', token);

            axios.post(`${Url}/api/v1/admin/operate/question/exam`, formData)
                .then(res => {

                    getDataPaket()
                    if (res.data.status == 'success') {
                        Swal.fire(
                            'Selamat!',
                            'Paket Berhasil Ditambahkan',
                            'success'
                        )
                    }
                    else {
                        Swal.fire(
                            'Oopsss...',
                            'Terjadi Kesalahan, Coba Lagi',
                            'error'
                        )
                    }
                })
        }
    }

    function klikSimpanEditPaket(value) {

        if (durasiWaktu === "00:00") {
            // console.log("gagal");
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Durasi Wajib Diisi',
            })
        }
        else {

            // console.log(value);
            const formData = new FormData();

            var a = durasiWaktu.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            var minute = (+a[0]) * 60 + (+a[1]);
            console.log(minute);

            formData.append('id', idPaket);
            formData.append('name', namaPaket);
            formData.append('time', minute);
            formData.append('option', 'pre-test');
            formData.append('token', token);

            axios.post(`${Url}/api/v1/admin/operate/question/exam`, formData)
                .then(res => {

                    getDataPaket()
                    if (res.data.status == 'success') {
                        Swal.fire(
                            'Selamat!',
                            'Paket Berhasil Ditambahkan',
                            'success'
                        );
                        setModalEditExam(false);
                    }
                    else {
                        Swal.fire(
                            'Oopsss...',
                            'Terjadi Kesalahan, Coba Lagi',
                            'error'
                        );
                        setModalEditExam(false)
                    }
                })
        }
    }
    const data =
        [...dataPaket.map((item, i) => ({
            key: i + 1,
            nama: item.name,
            durasi: item.time,
            action:
                <div className='d-flex '>
                    <Button type="primary" icon={<PlusOutlined />} className='me-4' primary onClick={() => navigate('/createpretest/question?id=' + item.id)}></Button>
                    <Button type="primary" icon={<EditOutlined />} className='me-4' primary onClick={() => editExam(item.id)}></Button>
                    <Button type="primary" icon={<DeleteOutlined />} className='me-4' primary onClick={() => deleteExam(item.id)}></Button>
                </div>

        }))
        ];

    // tabel 
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() => clearFilters && handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: '#ffc069',
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    // tabel 

    const columns = [
        {
            title: 'No.',
            dataIndex: 'key',
            key: 'key',
            width: '10%',
            className: 'text-center',
            ...getColumnSearchProps('key'),
        }, {
            title: 'Name',
            dataIndex: 'nama',
            key: 'nama',
            width: '30%',
            className: 'text-center',
            ...getColumnSearchProps('nama'),
        },
        {
            title: 'Duration',
            dataIndex: 'durasi',
            key: 'durasi',
            width: '30%',
            className: 'text-center',
            ...getColumnSearchProps('durasi'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            width: '30%',
            className: 'text-center',
            ...getColumnSearchProps('action'),
        },
    ];

    return (
        <div className="container-fluid page-body-wrapper">
            <Sidebar />
            <div className="main-panel">
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">

                                        <div className="col-12 grid-margin stretch-card">
                                            <div className="card">
                                                <div className="card-body">
                                                    <div className='d-flex'>
                                                        <h4 className="card-title me-3">Buat Soal Pre-Test Baru</h4>
                                                        <svg xmlns="http://www.w3.org/2000/svg" role="button" href="#collapseExample" data-bs-toggle="collapse" width="20" height="20" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
                                                        </svg>
                                                    </div>
                                                    <form className="forms-sample collapse" id="collapseExample">
                                                        <div class="mb-3 row">
                                                            <label for="staticEmail" class="col-sm-2 col-form-label">Nama Test</label>
                                                            <div class="col-sm-10">
                                                                <input type="textarea" className="form-control" id="exampleInputEmail3" placeholder="Masukkan Nama Paket" onChange={(e) => setNamaPaket(e.target.value)} />
                                                            </div>
                                                        </div>
                                                        <div class="mb-3 row">
                                                            <label for="staticEmail" class="col-sm-2 col-form-label">Durasi Waktu</label>
                                                            <div class="col-sm-10">
                                                                <TimePicker
                                                                    defaultValue={moment('00:00', format)}
                                                                    format={format}
                                                                    onChange={(time, timeString) => {
                                                                        setDurasiWaktu(timeString);
                                                                    }}
                                                                    clearIcon={false}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className='text-end'>
                                                            <button type="button" className="btn btn-primary me-2 text-white" onClick={klikSimpanPaket}>Submit</button>
                                                            <button className="btn btn-light">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="col-lg-12 grid-margin stretch-card">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h4 className="card-title">List Paket Pre Test</h4>
                                                    <div className="table-responsive pt-3">
                                                        <Table pagination={{ pageSize: 5 }}
                                                            loading={loading ? true : false} columns={columns} dataSource={data}></Table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <Modal
                                            title="Edit Paket"
                                            centered
                                            visible={modalEditExam}
                                            onOk={() => klikSimpanEditPaket()}
                                            onCancel={() => setModalEditExam(false)}
                                        >
                                            {/* <div className="card">
                                                <div className="card-body"> */}
                                            <form className="forms-sample">
                                                <div class="mb-3 row">
                                                    <label for="staticEmail" class="col-sm-2 col-form-label">Nama Test</label>
                                                    <div class="col-sm-10">
                                                        <input type="textarea" defaultValue={dataNamaPaketId} className="form-control" id="exampleInputEmail3" placeholder="Masukkan Nama Paket" onChange={(e) => setNamaPaket(e.target.value)} />
                                                    </div>
                                                </div>
                                                <div class="mb-3 row">
                                                    <label for="staticEmail" class="col-sm-2 col-form-label">Durasi Waktu</label>
                                                    <div class="col-sm-10">
                                                        <TimePicker
                                                            defaultValue={moment('00:00', format)}
                                                            format={format}
                                                            onChange={(time, timeString) => {
                                                                setDurasiWaktu(timeString);
                                                            }}
                                                            clearIcon={false}
                                                        />
                                                    </div>
                                                </div>
                                                {/* <div className='text-end'>
                                                            <button type="button" className="btn btn-primary me-2 text-white" onClick={klikSimpanEditPaket}>Submit</button>
                                                            <button className="btn btn-light">Cancel</button>
                                                        </div> */}
                                            </form>
                                            {/* </div>
                                            </div> */}
                                        </Modal>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer className="footer">
                    <div className="d-sm-flex justify-content-center justify-content-sm-between">
                        <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
                        <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                    </div>
                </footer>
            </div >
        </div >

    )
}
export default SoalPreTestAdmin;
