import React, { Component } from 'react'
import logo from "../../../asset/logo.png";

export default class Header extends Component {
    render() {
        return (
            <div className="container-scroller">
                <nav className="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
                    <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
                        <div className="me-3">
                            <button className="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
                                <span className="icon-menu" />
                            </button>
                        </div>
                        <div>
                            <a className="navbar-brand brand-logo" href="index.html">
                                <img src={logo} alt="logo" />
                            </a>
                            <a className="navbar-brand brand-logo-mini" href="index.html">
                                <img src="images/logo-mini.svg" alt="logo" />
                            </a>
                        </div>
                    </div>
                    <div className="navbar-menu-wrapper d-flex align-items-top">
                        <ul className="navbar-nav">
                            <li className="nav-item font-weight-semibold d-none d-lg-block ms-0">
                                <h1 className="welcome-text"><span className="text-black fw-bold">Halaman Admin</span></h1>
                                {/* <h3 className="welcome-sub-text">Your performance summary this week </h3> */}
                            </li>
                        </ul>
                        <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
                            <span className="mdi mdi-menu" />
                        </button>
                    </div>
                </nav>
            </div>
        )
    }
}
