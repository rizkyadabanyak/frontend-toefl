import React, { Component } from 'react'
import { useNavigate } from 'react-router-dom';
import Sidebar from "../component/Sidebar";

function Menu() {
  let navigate = useNavigate();
  return (
    // <div>
    // <Sidebar/>
    <div className="main-panel">
      <div className="content-wrapper">
        <div className="row">
          <div className="col-sm-12">
            <div className="home-tab">
              <div className="tab-content tab-content-basic">
                <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                  <div className="row">
                    <div className="col-lg-12 d-flex flex-column">
                      {/* <div className="row flex-grow">
                            <div className="col-12 grid-margin stretch-card">
                              <div className="card card-rounded">
                                <div className="card-body">
                                  <div className="d-sm-flex justify-content-between align-items-start">
                                    <div>
                                      <h4 className="card-title card-title-dash">Market Overview</h4>
                                      <p className="card-subtitle card-subtitle-dash">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                                    </div>
                                    <div>
                                      <div className="dropdown">
                                        <button className="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> This month </button>
                                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                          <h6 className="dropdown-header">Settings</h6>
                                          <a className="dropdown-item" href="#">Action</a>
                                          <a className="dropdown-item" href="#">Another action</a>
                                          <a className="dropdown-item" href="#">Something else here</a>
                                          <div className="dropdown-divider" />
                                          <a className="dropdown-item" href="#">Separated link</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="d-sm-flex align-items-center mt-1 justify-content-between">
                                    <div className="d-sm-flex align-items-center mt-4 justify-content-between"><h2 className="me-2 fw-bold">$36,2531.00</h2><h4 className="me-2">USD</h4><h4 className="text-success">(+1.37%)</h4></div>
                                    <div className="me-3"><div id="marketing-overview-legend" /></div>
                                  </div>
                                  <div className="chartjs-bar-wrapper mt-3">
                                    <canvas id="marketingOverview" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div> */}
                      <div className="col-lg-12 grid-margin stretch-card">
                        <div className="card">
                          <div className="card-body">
                            <h4 className="card-title">Striped Table</h4>
                            <p className="card-description">
                              Add class <code>.table-striped</code>
                            </p>
                            <div className="table-responsive">
                              <table className="table table-striped">
                                <thead>
                                  <tr>
                                    <th>
                                      User
                                    </th>
                                    <th>
                                      First name
                                    </th>
                                    <th>
                                      Progress
                                    </th>
                                    <th>
                                      Amount
                                    </th>
                                    <th>
                                      Deadline
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face1.jpg" alt="image" />
                                    </td>
                                    <td>
                                      Herman Beck
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-success" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $ 77.99
                                    </td>
                                    <td>
                                      May 15, 2015
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face2.jpg" alt="image" />
                                    </td>
                                    <td>
                                      Messsy Adam
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-danger" role="progressbar" style={{ width: '75%' }} aria-valuenow={75} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $245.30
                                    </td>
                                    <td>
                                      July 1, 2015
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face3.jpg" alt="image" />
                                    </td>
                                    <td>
                                      John Richards
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-warning" role="progressbar" style={{ width: '90%' }} aria-valuenow={90} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $138.00
                                    </td>
                                    <td>
                                      Apr 12, 2015
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face4.jpg" alt="image" />
                                    </td>
                                    <td>
                                      Peter Meggik
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-primary" role="progressbar" style={{ width: '50%' }} aria-valuenow={50} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $ 77.99
                                    </td>
                                    <td>
                                      May 15, 2015
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face5.jpg" alt="image" />
                                    </td>
                                    <td>
                                      Edward
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-danger" role="progressbar" style={{ width: '35%' }} aria-valuenow={35} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $ 160.25
                                    </td>
                                    <td>
                                      May 03, 2015
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face6.jpg" alt="image" />
                                    </td>
                                    <td>
                                      John Doe
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-info" role="progressbar" style={{ width: '65%' }} aria-valuenow={65} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $ 123.21
                                    </td>
                                    <td>
                                      April 05, 2015
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="py-1">
                                      <img src="../../images/faces/face7.jpg" alt="image" />
                                    </td>
                                    <td>
                                      Henry Tom
                                    </td>
                                    <td>
                                      <div className="progress">
                                        <div className="progress-bar bg-warning" role="progressbar" style={{ width: '20%' }} aria-valuenow={20} aria-valuemin={0} aria-valuemax={100} />
                                      </div>
                                    </td>
                                    <td>
                                      $ 150.00
                                    </td>
                                    <td>
                                      June 16, 2015
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* content-wrapper ends */}
      {/* partial:partials/_footer.html */}
      <footer className="footer">
        <div className="d-sm-flex justify-content-center justify-content-sm-between">
          <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
          <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
        </div>
      </footer>
      {/* partial */}
    </div>
    // </div>

  )
}
export default Menu;
