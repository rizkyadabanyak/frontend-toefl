// import Navbar from "../../landingpage/component/Navbar";
import Header from "../component/Header";
import Menu from "../component/Menu";
import Sidebar from "../component/Sidebar";

function ViewDashboard() {
  return (
    <div>
      <Header/>
      <div className="container-fluid page-body-wrapper">
        <Sidebar/>
        <Menu/>
      </div>
    </div>
  );
}

export default ViewDashboard;
