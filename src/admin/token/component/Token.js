import axios from 'axios';
import React, { Component, useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { LoadingOutlined, SearchOutlined } from "@ant-design/icons";
import Swal from 'sweetalert2';
import Sidebar from '../component/Sidebar';
import { Spin, Button, Input, Space, Table, Tag } from 'antd';
import "../style/token.css";
import Highlighter from 'react-highlight-words';
import Url from "../../../config";


function Token() {
    let navigate = useNavigate();
    let token = sessionStorage.getItem("token");
    const [kodeReferral, setKodeReferral] = useState();
    const [loadKode, setLoadKode] = useState(false);
    const [loading, setLoading] = useState(true);
    const [listReferral, setListReferral] = useState([]);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const searchInput = useRef(null);

    useEffect(() => {

        let formData = new FormData();
        formData.append('token', token);

        axios.post(`${Url}/api/v1/admin/operate/referral/index`, formData)
            .then(res => {
                if (res.data.status == 'success') {

                    setListReferral(res.data.data);
                }
                else {
                    Swal.fire(
                        'Gagal!',
                        res.data.message,
                        'error'
                    )
                }
                setLoading(false)

            })
    }, [kodeReferral])

    function buatKode() {
        setLoadKode(true);
        let formData = new FormData();
        formData.append('token', token);

        axios.post(`${Url}/api/v1/admin/operate/referral/store`, formData)
            .then(res => {
                console.log(res);
                setKodeReferral(res.data.data.referral);

            }).then(() => setLoadKode(false));
    }

    const data =
        [...listReferral.map((item, i) => ({
            key: i + 1,
            code: item.referral,
            status:
                <>
                    {
                        item.status == 'active' ?
                            <Tag className="text-center" color="success">{item.status}</Tag> :
                            <Tag color="error" className="text-center">{item.status}</Tag>
                    }
                </>
            ,
            tgl: item.created_at.substring(0, 10),


        }))
        ];

    // tabel 
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() => clearFilters && handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: '#ffc069',
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    // tabel 

    const columns = [
        {
            title: 'No.',
            dataIndex: 'key',
            key: 'key',
            width: '10%',
            className: 'text-center',
            ...getColumnSearchProps('key'),
        }, {
            title: 'Kode Registrasi',
            dataIndex: 'code',
            key: 'code',
            width: '20%',
            className: 'text-center',
            ...getColumnSearchProps('code'),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            width: '20%',
            className: 'text-center',
            ...getColumnSearchProps('status'),
        },
        {
            title: 'Tanggal Buat',
            dataIndex: 'tgl',
            key: 'tgl',
            width: '25%',
            className: 'text-center',
            ...getColumnSearchProps('tgl'),
        },
    ];



    return (
        <div className="container-fluid page-body-wrapper">
            {/* partial:partials/_settings-panel.html */}
            {/* partial */}
            {/* partial:partials/_sidebar.html */}
            {/* <nav className="sidebar sidebar-offcanvas" id="sidebar">
                <ul className="nav">
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={() => { navigate("/viewdashboard"); }}>
                            <i className="mdi mdi-grid-large menu-icon" />
                            <span className="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={() => { navigate("/viewdashboard"); }}>
                            <i className="mdi mdi-grid-large menu-icon" />
                            <span className="menu-title">Soal</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={() => { navigate("/viewdashboard"); }}>
                            <i className="mdi mdi-grid-large menu-icon" />
                            <span className="menu-title">Penilaian</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={() => { navigate("/viewtoken"); }}>
                            <i className="mdi mdi-grid-large menu-icon" />
                            <span className="menu-title">List Token</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={() => { navigate("/viewdashboard"); }}>
                            <i className="mdi mdi-grid-large menu-icon" />
                            <span className="menu-title">User</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="" onClick={() => { navigate("/registeradmin"); }}>
                            <i className="mdi mdi-grid-large menu-icon" />
                            <span className="menu-title">Registrasi Admin</span>
                        </a>
                    </li>
                </ul>
            </nav> */}

            <Sidebar />

            <Spin className='loading' tip="Loading..." size="large" style={{ display: loading ? "blok" : "none" }} />
            <div className="main-panel" style={{ display: loading ? "none" : "flex" }}>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">

                                        <div className="col-12 grid-margin stretch-card">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h4 className="card-title">Buat Kode Registrasi</h4>
                                                    <form className="forms-sample">
                                                        <button type="button" onClick={buatKode} className="text-white btn btn-primary me-2">Buat Kode</button>
                                                    </form>
                                                    <h4 className="d-flex justify-content-start"><LoadingOutlined style={{ display: loadKode ? "block" : "none" }} /></h4>
                                                    <h4 className="card-title" style={{ display: loadKode ? "none" : "block" }}>{kodeReferral}</h4>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-12 grid-margin stretch-card">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="card-title text-start">List Kode Referral</h4>
                                    <div className="table-responsive pt-3">

                                        <Table columns={columns} dataSource={data} />
                                        {/* <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Kode Referral
                                                    </th>
                                                    <th>
                                                        Status
                                                    </th>
                                                    <th>
                                                        Created at
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    listReferral.map((item, i) => (
                                                        <tr>
                                                            <td>{i + 1}</td>
                                                            <td>{item.referral}</td>
                                                            {
                                                                item.status == 'active' ?
                                                                    <td><Tag color="success">{item.status}</Tag></td> :
                                                                    <td><Tag color="error">{item.status}</Tag></td>
                                                            }
                                                            <td>{item.created_at.substring(0, 10)}</td>
                                                        </tr>
                                                    ))
                                                }

                                            </tbody>
                                        </table> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* content-wrapper ends */}
                {/* partial:partials/_footer.html */}
                <footer className="footer">
                    <div className="d-sm-flex justify-content-center justify-content-sm-between">
                        <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
                        <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                    </div>
                </footer>
                {/* partial */}
            </div>
            {/* main-panel ends */}
        </div>

    )
}
export default Token;
