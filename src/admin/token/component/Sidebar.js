import axios from 'axios';
import React, { Component } from 'react'
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Url from "../../../config"

function Sidebar() {
    let navigate = useNavigate();
    let token = sessionStorage.getItem("token");

    function logoutAdmin(){
        const formData = new FormData();
        formData.append('token' , token);
        axios.post(`${Url}/api/v1/admin/operate/logout`, formData)
        .then((res) => {
            if(res.data.status == 'success'){
                sessionStorage.removeItem("token");
                navigate('/lingvilletest-admin/login')
            }
            else{
                Swal.fire(
                    'Gagal Logout', 
                    res.data.message,
                    'error'
                )
            }
        })
    }
    return (
        <div>
            <nav className="sidebar sidebar-offcanvas" id="sidebar">
                <ul className="nav">
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#form-elements-soal" aria-expanded="false" aria-controls="form-elements">
                            <i class="menu-icon mdi mdi-card-text-outline"></i>
                            <span class="menu-title">Soal</span>
                            <i class="menu-arrow"  ></i>
                        </a>
                        <div class="collapse" id="form-elements-soal">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"><a class="nav-link" type="button" onClick={() => { navigate("/createpretest"); }}>Pre-Test</a></li>
                            </ul>
                        </div>
                        <div class="collapse" id="form-elements-soal">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"><a class="nav-link" type="button" onClick={() => { navigate("/createtoefl"); }}>TOEFL</a></li>
                            </ul>
                        </div>
                    </li>
                    <li className="nav-item active">
                        <a className="nav-link" type="button" onClick={() => { navigate("/viewtoken"); }}>
                            <i className="mdi mdi-key-variant menu-icon" />
                            <span className="menu-title">List Token</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" type="button" onClick={() => { navigate("/datauser"); }}>
                            <i className="mdi mdi-account-check menu-icon" />
                            <span className="menu-title">User</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" type="button" onClick={() => { navigate("/history"); }}>
                            <i className="mdi mdi-database menu-icon" />
                            <span className="menu-title">History</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" type="button" onClick={()=> logoutAdmin()}>
                            <i className="mdi mdi-logout menu-icon" />
                            <span className="menu-title">Logout</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    )
}
export default Sidebar;
