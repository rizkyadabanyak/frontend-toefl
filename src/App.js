// import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import LandingPage from './user/landingpage/view/LandingPage';
import ViewLogin from './user/login/view/ViewLogin';
import ViewRegister from './user/register/view/ViewRegister';
import ViewProfile from './user/profil/view/ViewProfile';
import EditPassword from './user/profil/view/ViewEditPassword';
import ViewDashboard from './admin/dasboard/view/ViewDashboard';
import HistoryTest from './user/historytest/view/HistoryTest';
import ViewLoginAdmin from './admin/login/view/ViewLogin';
import ViewToken from './admin/token/view/ViewToken';
import ViewPreTest from './user/soal/view/ViewSoalPreTest';
import ViewToefl from './user/soal/view/ViewSoalToefl';
import ViewDetailHistory from './admin/register/view/ViewDetailHistory';
import ViewSoalPreTestAdmin from './admin/soal/view/ViewSoalPreTestAdmin';
import ViewSoalTeoflAdmin from './admin/soal/view/ViewSoalToeflAdmin';
import ViewJenisTes from './user/jenis tes/view/ViewJenisTes';
import ViewDataUser from './admin/user/view/ViewDataUser';
import ViewPenilaianToefl from './admin/penilaian/view/ViewPenilaianToefl';
import ViewPenilaianPreTest from './admin/penilaian/view/ViewPenilaianPreTest';
import ViewPacketToeflAdmin from './admin/soal/view/ViewSoalToeflAdmin';
import BuatSoalToefl from './admin/soal/view/BuatSoalToefl';
import BuatSoalPretest from './admin/soal/view/BuatSoalPretest';
import ViewJenisPreTest from './user/jenis tes/view/ViewJenisPreTest';
import ViewHistoryAdmin from './admin/register/view/ViewHistory';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<LandingPage />}></Route>
          <Route path='/login' element={<ViewLogin />}></Route>
          <Route path='/register' element={<ViewRegister />}></Route>
          <Route path='/profile' element={<ViewProfile />}></Route>
          <Route path='/editpassword' element={<EditPassword />}></Route>
          <Route path='/viewdashboard' element={<ViewDashboard />}></Route>
          <Route path='/historytest' element={<HistoryTest />}></Route>
          <Route path='/lingvilletest-admin/login' element={<ViewLoginAdmin />}></Route>
          <Route path='/history' element={<ViewHistoryAdmin />}></Route>
          <Route path='/detailhistory' element={<ViewDetailHistory />}></Route>
          <Route path='/viewtoken' element={<ViewToken />}></Route>
          <Route path='/pretest' element={<ViewPreTest />}></Route>
          <Route path='/toefl' element={<ViewToefl />}></Route>
          <Route path='/admin' element={<ViewDashboard />}></Route>
          <Route path='/createpretest' element={<ViewSoalPreTestAdmin />}></Route>
          <Route path='/createpretest/question' element={<BuatSoalPretest />}></Route>
          <Route path='/createtoefl' element={<ViewPacketToeflAdmin />}></Route>
          <Route path='/createtoefl/question' element={<BuatSoalToefl />}></Route>
          <Route path='/jenis-paket-toefl' element={<ViewJenisTes />}></Route>
          <Route path='/jenis-paket-pretest' element={<ViewJenisPreTest />}></Route>
          <Route path='/datauser' element={<ViewDataUser />}></Route>
          <Route path='/penilaiantoefl' element={<ViewPenilaianToefl />}></Route>
          <Route path='/penilaianpretest' element={<ViewPenilaianPreTest />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
