import Register from "../component/JenisTes";
import Navbar from "../../landingpage/component/Navbar";
import JenisTes from "../component/JenisTes";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import Url from "../../../config.js";
import JenisPreTest from "../component/JenisPreTest";
// import "../style/Landingpage.css";

function ViewJenisPreTest() {

  
  let navigate = useNavigate();
  let token = sessionStorage.getItem("token");
  const [nama, setNama] = useState();
  const [loading, setLoading] = useState(true);


  useEffect(() => {
    let formData = new FormData();

    formData.append('token', token);
    axios.post(`${Url}/api/v1/student/operate/profile`, formData)
      .then(res => {
        if (res.data.status == "danger") {
          Swal.fire(
            'Gagal!',
            'Anda Belum Login',
            'error'
          )
          navigate("/");
        }
        else if(res.data.status == "success") {
          setNama(res.data.data.name);
          
          setLoading(false);
        }
      })
  }, []);

  if (loading) {
    return (
      <div></div>
    )
  }


  return (
    <div className="viewregister">
      <Navbar userName={nama}/>
      <JenisPreTest/>
    </div>
  );
}

export default ViewJenisPreTest;
