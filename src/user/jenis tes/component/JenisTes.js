import "../style/JenisTes.css";
import bintang from "../../../asset/Bintang.png";
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import Url from "../../../config"
import { Col, Row } from "antd";

function JenisTes() {
    let navigate = useNavigate();
    let token = sessionStorage.getItem("token");
    const [dataJenis, setDataJenis] = useState([]);
    const [loading, setLoading] = useState(true)


    useEffect(() => {
        let formData = new FormData();

        formData.append('token', token);
        formData.append('type', 'toefl');
        axios.post(`${Url}/api/v1/student/operate/show/exam`, formData)
            .then(res => {
                setDataJenis(res.data.data)
                setLoading(false);

            })
    }, []);

    function pilihPaket(id) {
        let formData = new FormData();

        formData.append('token', token);
        formData.append('category_id', id);
        axios.post(`${Url}/api/v1/student/operate/submitExam`, formData)
            .then(res => {
                if(res.data.status=='success'){
                    
                    sessionStorage.setItem("status", res.data.data.status);
                    
                    sessionStorage.setItem("session", res.data.data.session);
                    sessionStorage.setItem("jawaban",[]);
                    sessionStorage.setItem("waktu", []);
                    navigate("/toefl")
                }
                else {
                    Swal.fire(
                        'Gagal!',
                        res.data.message,
                        'error')
                }

            })
    }

    return (
        <div className="jenis-tes container">
            <div className="text-pilih">Pilih Paket</div>
         
            <Row gutter={{
                xs: 8,
                sm: 16,
                md: 24,
                lg: 32,
            }}>
                {
                    dataJenis.map((item, i) => (

                        <Col className="gutter-row" type="button" span={6} onClick={() => pilihPaket(item.id)}>
                            <div className="mt-4 text-white kotak-jenis">
                                <div style={{fontWeight: "bold"}}>{item.name}</div>
                                <div>{item.time} Menit</div>
                            </div>
                        </Col>
                    ))
                }

            </Row>

        </div>
    );
}

export default JenisTes;
