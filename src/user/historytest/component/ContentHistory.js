import axios from "axios";
import { useEffect, useState } from "react";
import "../style/ContentHistory.css";
import Url from "../../../config"
import { useNavigate } from "react-router-dom";

function ContentHistory() {
    let token = sessionStorage.getItem("token");
    const [dataHistory, setDataHistory] = useState([]);
    const navigate = useNavigate()
    // const date = new Date()
    const formData = new FormData();
    formData.append('token', token)
    useEffect(() => {
        axios.post(`${Url}/api/v1/student/operate/history/exam`, formData)
            .then(res => {
                setDataHistory(res.data.data)
            })
    }, [])

    function lanjutkanTes(session, jenis) {
        sessionStorage.setItem("waktu", []);
        sessionStorage.setItem("session", session);

        const formData = new FormData();
        formData.append('token', token)
        formData.append('session', session)
        axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, formData)
            .then(res => {
                console.log(res.data)
                sessionStorage.setItem("jawaban", res.data.tmpAnswer);

                if (jenis == 'pre-test') {

                    navigate("/pretest")
                }
                else {
                    navigate("/toefl")
                }
            })
    }
    return (



        <div className=" d-flex container content-history justify-content-between">

            <div className="d-flex flex-column align-items-start  " style={{ width: "100%" }}  >
                <div className="judul-header-history">Detail History</div>

                {
                    dataHistory.map((item, i) => (
                        <div className="card-history d-flex flex-column mb-3" >
                            <div className="d-flex justify-content-between align-items-center" >
                                <div className="judul-history ">{item.categories.categories.slice(0, 1).toUpperCase() + item.categories.categories.slice(1)}</div>
                                {item.status == "finished" ?
                                    <div className="status-history ">{item.status.slice(0, 1).toUpperCase() + item.status.slice(1)}</div> :

                                    item.status == "on-going" ?
                                        <div type="button" className="status-ongoing " onClick={() => lanjutkanTes(item.session, item.categories.categories)}>Lanjutkan Tes</div> :

                                        <div type="button" className="status-failed " >{item.status.slice(0, 1).toUpperCase() + item.status.slice(1)}</div>



                                }
                            </div>
                            <div>
                                <table className="isi-history">
                                    <tr>
                                        <td width="100px" className="text-start">Paket</td>
                                        <td width="20px" className="text-start">:</td>
                                        <td className="text-start">{item.categories.name}</td>
                                    </tr>
                                    <tr>
                                        <td width="100px" className="text-start">Jadwal Tes</td>
                                        <td width="20px" className="text-start">:</td>
                                        <td className="text-start">{new Date(item.updated_at).toLocaleDateString("id")}</td>
                                    </tr>
                                    <tr>
                                        <td width="100px" className="text-start">Nilai Akhir</td>
                                        <td width="20px" className="text-start">:</td>
                                        <td className="text-start">{Math.ceil(item.score)}</td>
                                    </tr>
                                    {/* <tr>
                                        <td width="100px" valign="top" className="text-start">Nilai</td>
                                        <td width="20px" valign="top" className="text-start">:</td>
                                        <td className="text-start">
                                            <ul>
                                                <li>Listening 410</li>
                                                <li>Listening 410</li>
                                                <li>Listening 410</li>
                                            </ul>
                                        </td>
                                    </tr> */}
                                </table>
                            </div>
                        </div>

                    ))
                }

            </div>

        </div>


    );
}

export default ContentHistory;
