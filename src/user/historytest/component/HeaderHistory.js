import { useNavigate } from "react-router-dom";
import "../style/HeaderHistory.css";



export const HeaderHistory = ({ userName, data }) => {
    const navigate = useNavigate()
    return (
        <div className=" d-flex col container header-history justify-content-between">
            <div className="d-flex col-8 flex-column align-items-start "  >
                <div className="judul-header-history">Status Test</div>
                <div className="card-status-test d-flex flex-column align-items-start">
                    <div className="nama-card-status">Hai, {userName}</div>
                    {/* <div className="d-flex mt-2">
                        <div className="status-tes">Selamat</div>
                        <div className="ket-status-tes">Anda Telah Lolos Tes</div>
                    </div> */}
                    <div className="d-flex flex-column align-items-start" style={{ width: "100%" }}>
                        <div className="text-isi">Dengan Skor Tertinggi :</div>
                        <div className="d-flex justify-content-between align-items-center" style={{ width: "100%" }} >
                            <div className="skor-toefl">{Math.ceil(data.maxScore)}</div>
                            <div className="btn-tes btn" type="button" onClick={() => navigate("/jenis-paket-pretest")}>Tes Lagi</div>
                        </div>
                    </div>


                </div>
            </div>
            <div className="d-flex flex-column align-items-start col-3 ">
                <div className="judul-header-history">Jumlah History</div>
                <div className="box-jumlah text-start">
                    <div className="total-tes">{data.totalPre_test}</div>
                    <div className="ket-total">Total PreTest</div>

                </div>
                <div className="box-jumlah text-start mt-2">
                    <div className="total-tes">{data.totalToefl}</div>
                    <div className="ket-total">Total Tes Toefl</div>

                </div>
            </div>
        </div>
    );
}

export default HeaderHistory;
