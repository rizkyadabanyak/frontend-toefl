
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../../landingpage/component/Navbar";
import ContentHistory from "../component/ContentHistory";
import HeaderHistory from "../component/HeaderHistory";
import Url from "../../../config";

function HistoryTest() {

  let navigate = useNavigate();
  let token = sessionStorage.getItem("token");
  const [nama, setNama] = useState();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState()

  useEffect(() => {
    let formData = new FormData();

    formData.append('token', token);
    axios.post(`${Url}/api/v1/student/operate/profile`, formData)
      .then(res => {
        if (res.data.status == "danger") {
          Swal.fire(
            'Gagal!',
            'Anda Belum Login',
            'error'
          )
          navigate("/");
        }
        else if(res.data.status == "success") {
          setData(res.data);
          setNama(res.data.data.name);
          
          setLoading(false);
        }
      })
  }, []);

  if (loading) {
    return (
      <div></div>
    )
  }

  return (
    <div className="view-history">
      <Navbar userName={nama} />
      <HeaderHistory userName={nama} data={data}/>
      <ContentHistory  />
    </div>
  );
}

export default HistoryTest;
