import "../style/CardKursus.css";
import pretestimg from "../../../asset/testing.png";
import iconcentang from "../../../asset/centang.png";
import toeflimh from "../../../asset/eng.png";
import logo from "../../../asset/logo.png";
import { useNavigate } from "react-router-dom";

function CardKursus() {
    let navigate = useNavigate();
    return (
        <div className="card-kursus container" id="jenistes">
            <img className="mt-0" src={logo} height="80px" />
            {/* <div className="judul-landing">Jenis Paket Tes</div> */}
            <div className="col d-flex justify-content-between flex-column flex-md-row grup-paket">
                <div className="col-lg-6 col-md-12 card-paket">
                    <img src={pretestimg} className="img-paket" style={{marginTop: "0px"}}></img>
                    <div className="judul-paket">Pre-Test</div>
                    <div className="penjelasan-paket text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                    <button className="btn btn-pilih-paket" onClick={()=> navigate("/jenis-paket-pretest")}>Pilih Paket</button>

                    <div className="d-flex align-items-center text-start mt-4 ">
                        <div ><img src={iconcentang} width="25px" style={{ marginTop: "0px" }}></img></div>
                        <div className="ps-4">
                            <div className="judul-subpaket mt-3">Structure</div>
                            <div className="isi-subpaket">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        </div>
                    </div>
                    <div className="d-flex align-items-center text-start mt-4">
                        <div ><img src={iconcentang} width="25px" style={{ marginTop: "0px" }}></img></div>
                        <div className="ps-4">
                            <div className="judul-subpaket mt-3">Structure</div>
                            <div className="isi-subpaket">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        </div>

                    </div>
                </div>
                <div className="jarak"></div>
                <div className="col-lg-6 col-md-12 card-paket">
                    <img src={toeflimh} className="img-paket" style={{marginTop: "0px"}}></img>
                    <div className="judul-paket">Toefl</div>
                    <div className="penjelasan-paket text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                    <button className="btn btn-pilih-paket" onClick={()=>navigate("/jenis-paket-toefl")}>Pilih Paket</button>

                    <div className="d-flex align-items-center text-start mt-4 ">
                        <div ><img src={iconcentang} width="25px" style={{ marginTop: "0px" }}></img></div>
                        <div className="ps-4">
                            <div className="judul-subpaket mt-3">Listening</div>
                            <div className="isi-subpaket">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        </div>
                    </div>
                    <div className="d-flex align-items-center text-start mt-4 ">
                        <div ><img src={iconcentang} width="25px" style={{ marginTop: "0px" }}></img></div>
                        <div className="ps-4">
                            <div className="judul-subpaket mt-3">Structure</div>
                            <div className="isi-subpaket">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        </div>
                    </div>
                    <div className="d-flex align-items-center text-start mt-4">
                        <div ><img src={iconcentang} width="25px" style={{ marginTop: "0px" }}></img></div>
                        <div className="ps-4">
                            <div className="judul-subpaket mt-3">Structure</div>
                            <div className="isi-subpaket">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default CardKursus;
