import '../style/Navbar.css';
import logo from "../../../asset/logo.png";
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useEffect, useState } from 'react';
import axios from 'axios';

export const Navbar = ({ userName }) => {
    let navigate = useNavigate();
    let token = sessionStorage.getItem("token");

    function klikLogout() {
        let formData = new FormData();

        formData.append('token', token);
        fetch(`https://api.lingvilletest.com/api/v1/student/operate/logout`, {
            method: 'POST',
            credentials: 'same-origin',
            body: formData

        })
            .then(res => res.json())
            .then(data => {
                if (data.status == 'success') {
                    sessionStorage.removeItem("token");
                    navigate("/");
                }
                else {
                    Swal.fire(
                        'Gagal!',
                        data.message,
                        'error'
                    )
                }
            })
    }

    return (
        <div className="App">
            <nav class="navbar navbar-expand-lg lingville">
                <div class="container-fluid">
                    <div class="navbar-brand ms-5 me-5 " href="" onClick={() => navigate("/")}><img className="mt-0" src={logo} height="50px" /></div>
                    {/* <a class="navbar-brand ms-5 me-5" href="#">lingville</a> */}
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mb-2 mb-lg-0 me-auto">
                            <li class="nav-item me-5">
                                <a class="nav-link item-nav active "  aria-current="page" onClick={() => navigate("/")}>Home</a>
                            </li>
                            <li class="nav-item dropdown me-5">
                                <a class="nav-link item-nav dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Jenis Tes
                                </a>
                                <ul class="dropdown-menu">
                                    <li><span type="button" class="dropdown-item" onClick={() => navigate("/jenis-paket-pretest")}>Tes Bahasa Inggris TBI</span></li>
                                    <li><span type="button" class="dropdown-item" onClick={() => navigate("/jenis-paket-toefl")}>TOEFL</span></li>
                                </ul>
                            </li>
                           
                            <li class="nav-item me-5" >
                                <a class="nav-link item-nav active" aria-current="page" href="#tentangkami">Tentang Kami</a>
                            </li>
                        </ul>
                        <form class="d-flex me-5 justify-content-center" role="search">
                            {
                                token != null ? (

                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle button-userlogin text-white" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Hi, {userName}
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item btn" onClick={() => { navigate("/profile"); }}>Edit Profile</a></li>
                                            <li><a class="dropdown-item btn" onClick={() => { navigate("/historytest"); }}>History Test</a></li>
                                            <li><a class="btn dropdown-item" onClick={() => { klikLogout(); }}>Logout</a></li>
                                        </ul>
                                    </div>

                                    // <button class="btn button-login-navbar" type="button" onClick={() => { klikLogout(); }}>Hallo, </button>
                                ) : <button class="btn button-login-navbar" type="submit" onClick={() => { navigate("/login"); }}>Login</button>

                            }
                        </form>

                    </div>
                </div>
            </nav>
        </div>
    );
}

export default Navbar;
