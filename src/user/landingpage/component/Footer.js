import "../style/Footer.css";
import facebook from "../../../asset/facebook.png";
import linkedin from "../../../asset/linkedin.png";
import twitter from "../../../asset/twitter.png";

function Footer() {
    return (
        <div className="footer container col d-flex flex-column flex-md-row justify-content-center footer-lingville">
            <div className=" col kotak-footer margin-kotak-footer">
                <div className="judul-footer">Tentang EF SET</div>
                <div className="isi-footer text-start">
                    Tentang tes
                    Skala skor kami
                    Penelitian kami
                    FAQ
                </div>
            </div>
            <div className=" col kotak-footer margin-kotak-footer">
                <div className="judul-footer">Pelajari lebih jauh</div>
                <div className="isi-footer text-start" >
                    EF SET Certificate™
                    Tentang CEFR
                    Solusi penilaian untuk perusahaan dan sekolah
                </div>
            </div>
            <div className=" col kotak-footer margin-kotak-footer">
                <div className="judul-footer ">EF Belajar bahasa Inggris</div>
                <div className="isi-footer text-start">
                    Kursus bahasa Inggris di Indonesia
                    Kursus bahasa Inggris di luar negeri
                </div>
            </div>
            <div className=" col kotak-footer" style={{width: "inherit"}}>
                <div className="judul-footer">Hubungi kami</div>
                <div className="isi-footer text-start mb-2">Kontak</div>
                <div className="d-flex align-items-center">
                    <img src={facebook} height="15px" style={{marginTop: "0px"}}></img>
                    <div className="ms-2 sosmed-footer">Like us on Facebook</div>
                </div>
                <div className="d-flex align-items-center">
                    <img src={linkedin} height="15px" style={{marginTop: "0px"}}></img>
                    <div className="ms-2 sosmed-footer">Follow us on LinkedIn</div>
                </div>
                <div className="d-flex align-items-center">
                    <img src={twitter} height="15px" style={{marginTop: "0px"}}></img>
                    <div className="ms-2 sosmed-footer">Follow us on Twitter</div>
                </div>
            </div>
        </div>
    );
}

export default Footer;
