import '../style/Banner.css';
import banner from "../../../asset/banner.png";

function Banner() {
  return (
    // <div className='banner'>
    <div className="d-flex  col flex-md-column-reverse flex-lg-row-reverse  banner container flex-column flex-md-row">
      <div className='col-lg-6 col-md-12 ' >
        <img src={banner} className="gambar-banner" />
      </div>
      <div className="col-lg-6  col-md-12 banner-kiri text-start">
        <div className="judul-banner mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
        <div className="sub-judul-banner mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. A mauris dignissim dolor euismod nisl.</div>
        <button class="btn button-banner" type="submit">Tes Bahasa Inggris Sekarang</button>
      </div>

    </div>



    // </div>
  );
}

export default Banner;
