import "../style/PenjelasanWeb.css";
import penjelasanimg from "../../../asset/penjelasanimg.png";

function PenjelasanWeb() {
  return (
    <div className="penjelasan-web col container">
      <div className=" judul-landing">Penjelasan Web</div>
      <div className="col d-flex flex-column flex-md-row justify-content-between">
        <div className="col-lg-6 col-md-12 gambar-penjelasan"><img src={penjelasanimg} className="gambar-penjelasan" /></div>
        <div className="col-lg-6 col-md-12 isi-penjelasan">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper cursus mi, quis egestas fringilla elementum posuere viverra. Vitae enim congue sit neque egestas a at at. Elit vitae ultricies praesent quam velit. Phasellus porttitor ullamcorper tincidunt vel amet faucibus. Vivamus libero nisl pharetra amet.
        </div>
      </div>
    </div>
  );
}

export default PenjelasanWeb;
