import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Banner from "../component/Banner";
import CardKursus from "../component/CardKursus";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";
import PenjelasanWeb from "../component/PenjelasanWeb";
import "../style/Landingpage.css";

function LandingPage() {
  let navigate = useNavigate();
  let token = sessionStorage.getItem("token");
  const [nama, setNama] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let formData = new FormData();

    formData.append('token', token);
    axios.post(`https://api.lingvilletest.com/api/v1/student/operate/profile`, formData)
      .then(res => {
        if(res.data.status == "danger"){

          setLoading(false);
        }
        else{

          setNama(res.data.data.name);
          setLoading(false);
        }
      })
  }, []);

  if (loading) {
    return (
      <div></div>
    )
  }

  return (

    <div className="landingpage">
      <Navbar userName={nama}/>
      <Banner />
      <CardKursus />
      <PenjelasanWeb />
      <Footer />
    </div>
  );
}

export default LandingPage;
