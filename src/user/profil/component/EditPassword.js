import "../style/Profile.css";
import avatar from "../../../asset/avatar-1.png";
import { useNavigate } from 'react-router-dom';
import axios from "axios";
import { useEffect, useState } from "react";
import Url from "../../../config"
import Swal from "sweetalert2";
import { Tooltip, Button, Modal, Input } from 'antd';


function EditPassword() {
    let navigate = useNavigate();
    // const [dataToken, setToken] = useState();
    let token = sessionStorage.getItem("token");
    const [oldPassword, setOldPassword] = useState();
    const [newPassword, setNewPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [dataNama, setNama] = useState();
    const [dataEmail, setEmail] = useState();
    const [fotoProfile, setFoto] = useState();
    // const [fotoProfile, setFoto] = useState();
    const [dataProfile, setDataProfile] = useState();
    const [modalUploadFoto, setModalUploadFoto] = useState(false);
    const [dataToken, setToken] = useState();

    useEffect(() => {
        const task = async () => {
            await getDataProfile();
        }
        task();
    }, []);

    const getDataProfile = async () => {
        const formData = new FormData();

        formData.append('token', token);
        axios.post(`${Url}/api/v1/student/operate/profile`, formData)
            .then(res => {
                setNama(res.data.data.name);
                setEmail(res.data.data.email);
                setFoto(res.data.data.img);
                // setLoading(false);
                // setDataProfile(res.data.data);
                //   }
            })
    }

    function klikEditFoto(token) {
        console.log(token);
        setToken(token);
        setModalUploadFoto(true);
    }

    function addFoto() {
        const formData = new FormData();

        formData.append('img', fotoProfile);
        formData.append('token', dataToken);

        console.log(fotoProfile);

        axios.post(`${Url}/api/v1/student/operate/profile/edit/img`, formData)
            .then(res => {
                if (res.data.status == 'success') {
                    Swal.fire(
                        'Selamat!',
                        res.data.message,
                        'success'
                    )
                }
                else {
                    Swal.fire(
                        'Oopsss...',
                        res.data.message,
                        'error'
                    )

                }
                setModalUploadFoto(false);
                getDataProfile();
                setFoto();
            })
    }

    function editPassword() {

        if (confirmPassword != newPassword) {
            Swal.fire(
                'Oopsss...',
                'Konfirmasi Password Harus Sama!!',
                'error'
            )
        } else {
            const formData = new FormData();

            formData.append('oldPassword', oldPassword);
            formData.append('newPassword', newPassword);
            formData.append('token', token);

            axios.post(`${Url}/api/v1/student/operate/profile/reset/password`, formData)
                .then(res => {
                    console.log(res.status);
                    if (res.data.status == "success") {
                        Swal.fire(
                            'Selamat!',
                            res.data.message,
                            'success'
                        )

                    }
                    // setModalEditNama(false);
                    getDataProfile();
                    // setNama();
                },(errror) => {
                    Swal.fire(
                        'Oooopppss. . . .',
                        "Password Lama Salah",
                        'error'
                    )
                })
        }
    }
    return (
        <div className="d-flex justify-content-center mb-5">
            <div class="card text-center card-tes">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="true" href="" onClick={() => { navigate("/profile"); }}>Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="true" href="" onClick={() => { navigate("/editpassword"); }}>Edit Password</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body d-flex flex-sm-row flex-column">
                    <div className="d-flex flex-column ps-lg-5 pt-lg-3">
                        <div><img
                            alt="image"
                            src={fotoProfile == "default" ?
                                avatar :
                                Url + "/" + fotoProfile}
                            class="rounded-circle photo-profile" />
                        </div>
                        <div>
                            <a onClick={() => klikEditFoto(token)} className="edit-photo" type="button">Edit Foto Profil</a>
                            {/* <Tooltip title="Edit">
                                <Button type="primary" className="mt-3" shape='circle' icon={<EditOutlined />} onClick={() => klikEditFoto(token)} />
                            </Tooltip> */}
                        </div>
                    </div>
                    <form className="d-flex flex-column form-edit p-5">
                        <div class="form-email-edit mb-3">
                            <label for="exampleInputPassword1" class="form-label d-flex flex-row">Password Lama</label>
                            <input type="password" placeholder="Masukkan password" class="form-control align-middle" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={(e) => setOldPassword(e.target.value)} />
                        </div>
                        <div class="form-nama-edit mb-3">
                            <label for="exampleInputPassword1" class="form-label d-flex flex-row">Password Baru</label>
                            <input type="password" placeholder="Masukkan password" class="form-control d-flex flex-row" id="exampleInputPassword1" onChange={(e) => setNewPassword(e.target.value)} />
                        </div>
                        <div class="form-kode-edit mb-4">
                            <label for="exampleInputPassword1" class="form-label d-flex flex-row">Konfirmasi Password Baru</label>
                            <input type="password" placeholder="Masukkan password" class="form-control d-flex flex-row" id="exampleInputPassword1" onChange={(e) => setConfirmPassword(e.target.value)} />
                        </div>
                        <div className="d-flex justify-content-end">
                            <button type="button" class="btn button-edit-profile mb-5" onClick={editPassword} >Simpan</button>
                        </div>
                    </form>
                </div>

                <Modal
                    title="Upload Foto"
                    centered
                    visible={modalUploadFoto}
                    onOk={() => addFoto()}
                    onCancel={() => setModalUploadFoto(false)}
                >
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail3">Upload Foto</label>
                        <input type="file" className="form-control" id="exampleInputEmail3" placeholder="Upload Foto" onChange={(e) => setFoto(e.target.files[0])} />
                    </div>
                </Modal>
            </div>
        </div>
    );
}

export default EditPassword;
