import "../style/Profile.css";
import avatar from "../../../asset/avatar-1.png";
import { useNavigate } from 'react-router-dom';
import axios from "axios";
import { useEffect, useState } from "react";
import Url from "../../../config"
import Swal from "sweetalert2";
import { Tooltip, Button, Modal, Input } from 'antd';
import { EditOutlined } from "@ant-design/icons";



function Profile() {
    let token = sessionStorage.getItem("token");
    let navigate = useNavigate();
    // const [loading, setLoading] = useState(true);
    const [dataNama, setNama] = useState();
    const [dataEmail, setEmail] = useState();
    // const [minIndex, setMinIndex] = useState(0);
    // const [maxIndex, setMaxIndex] = useState(0);
    const [fotoProfile, setFoto] = useState();
    const [dataProfile, setDataProfile] = useState();
    const [modalUploadFoto, setModalUploadFoto] = useState(false);
    const [modalEditNama, setModalEditNama] = useState(false);
    const [nama, setName] = useState();
    const [foto, setPhoto] = useState();
    const [dataToken, setToken] = useState();

    useEffect(() => {
        const task = async () => {
            await getDataProfile();
        }
        task();
    }, []);

    const getDataProfile = async () => {
        const formData = new FormData();

        formData.append('token', token);
        axios.post(`${Url}/api/v1/student/operate/profile`, formData)
            .then(res => {
                setNama(res.data.data.name);
                setEmail(res.data.data.email);
                setFoto(res.data.data.img);
                // setLoading(false);
                // setDataProfile(res.data.data);
                //   }
            })
    }

    function klikEditFoto(token) {
        console.log(token);
        setToken(token);
        setModalUploadFoto(true);
    }

    function klikEditnama(token) {
        console.log(token);
        setToken(token);
        setModalEditNama(true);
    }

    function addFoto() {
        const formData = new FormData();

        formData.append('img', fotoProfile);
        formData.append('token', dataToken);

        console.log(fotoProfile);

        axios.post(`${Url}/api/v1/student/operate/profile/edit/img`, formData)
            .then(res => {
                if (res.data.status == 'success') {
                    Swal.fire(
                        'Selamat!',
                        res.data.message,
                        'success'
                    )
                }
                else {
                    Swal.fire(
                        'Oopsss...',
                        res.data.message,
                        'error'
                    )

                }
                setModalUploadFoto(false);
                getDataProfile();
                setFoto();
            })
    }

    function editNama() {
        const formData = new FormData();

        formData.append('name', dataNama);
        formData.append('token', dataToken);

        axios.post(`${Url}/api/v1/student/operate/profile/edit`, formData)
            .then(res => {
                if (res.data.status == 'success') {
                    Swal.fire(
                        'Selamat!',
                        res.data.message,
                        'success'
                    )

                }
                else {
                    Swal.fire(
                        'Oopsss...',
                        res.data.message,
                        'error'
                    )
                }
                setModalEditNama(false);
                getDataProfile();
                setNama();
            })
    }

    return (
        <div className="d-flex justify-content-center mb-5">
            <div class="card text-center card-tes">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="true" href="" onClick={() => { navigate("/profile"); }}>Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="true" href="" onClick={() => { navigate("/editpassword"); }}>Edit Password</a>
                        </li>
                    </ul>
                </div>
                < div class="card-body d-flex flex-sm-row flex-column">
                    <div className="d-flex flex-column ps-lg-5 pt-lg-3">
                        <div><img
                            alt="image"
                            src={
                                fotoProfile == "default" ?
                                    avatar :
                                    Url + "/" + fotoProfile
                            }
                            class="rounded-circle photo-profile" />
                        </div>
                        <div>
                            <a onClick={() => klikEditFoto(token)} className="edit-photo" type="button">Edit Foto Profil</a>
                            {/* <Tooltip title="Edit">
                                <Button type="primary" className="mt-3" shape='circle' icon={<EditOutlined />} onClick={() => klikEditFoto(token)} />
                            </Tooltip> */}
                        </div>
                    </div>
                    <form className="d-flex flex-column form-edit pb-5 pe-5 px-5 pt-3">
                        <div class="form-email-edit mb-3">
                            <label for="exampleInputEmail1" class="form-label d-flex flex-row">Email</label>
                            <input type="email" value={dataEmail} placeholder="Masukkan email" class="form-control align-middle" id="exampleInputEmail1" aria-describedby="emailHelp" readOnly />
                        </div>
                        <div class="form-nama-edit mb-3">
                            <label for="exampleInputPassword1" class="form-label d-flex flex-row">Nama</label>
                            <div className="d-flex flex-row">
                                <input type="text" value={dataNama} placeholder="Masukkan nama" class="form-control d-flex flex-row me-5" id="exampleInputPassword1" readOnly />
                                <Tooltip title="Edit">
                                    <Button type="primary" className='me-3' shape='circle' icon={<EditOutlined />} onClick={() => klikEditnama(token)} />
                                </Tooltip>
                            </div>
                        </div>
                    </form>



                </div>

                <Modal
                    title="Upload Foto"
                    centered
                    visible={modalUploadFoto}
                    onOk={() => addFoto()}
                    onCancel={() => setModalUploadFoto(false)}
                >
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail3">Upload Foto</label>
                        <input type="file" className="form-control" id="exampleInputEmail3" placeholder="Upload Foto" onChange={(e) => setFoto(e.target.files[0])} />
                    </div>
                </Modal>

                <Modal
                    title="Edit Nama"
                    centered
                    visible={modalEditNama}
                    onOk={() => editNama()}
                    onCancel={() => setModalEditNama(false)}
                >
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail3">Nama</label>
                        <Input size="large" placeholder="Masukkan nama" className='mt-2' defaultValue={dataNama} onChange={(e) => setNama(e.target.value)} />

                        {/* <input type="file" className="form-control" id="exampleInputEmail3" placeholder="Upload Foto" onChange={(e) => setPhoto(e.target.files)} /> */}
                    </div>
                </Modal>

            </div>
        </div >
    );
}

export default Profile;
