import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../../landingpage/component/Navbar";
import Profile from "../component/Profile";
// import "../style/Landingpage.css";
import Url from "../../../config"

function ViewProfile() {

  let navigate = useNavigate();
  let token = sessionStorage.getItem("token");
  const [nama, setNama] = useState();
  const [loading, setLoading] = useState(true);


  useEffect(() => {
    let formData = new FormData();

    formData.append('token', token);
    axios.post(`${Url}/api/v1/student/operate/profile`, formData)
      .then(res => {
        if (res.data.status == "danger") {
          Swal.fire(
            'Gagal!', 
            'Anda Belum Login',
            'error'
          )
          navigate("/");
        }
        else if(res.data.status == "success") {
          setNama(res.data.data.name);
          
          setLoading(false);
        }
      })
  }, []);

  if (loading) {
    return (
      <div></div>
    )
  }


  return (
    <div className="viewprofile">
      <Navbar userName={nama}/>
      <Profile />
    </div>
  );
}

export default ViewProfile;
