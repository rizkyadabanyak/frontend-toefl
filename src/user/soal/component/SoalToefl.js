import "../style/SoalToefl.css";
import audiotes from "../../../asset/005.mp3";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
// import { useNavigate } from 'react-router-dom';
import Url from "../../../config";

import Countdown from 'react-countdown';
// import type { RadioChangeEvent } from 'antd';
import { RadioChangeEvent, Button, Pagination, Radio, Space, Spin, Tag } from 'antd';
import Swal from "sweetalert2";
import ReactHTMLParser from 'html-react-parser';
import {
    CheckCircleOutlined,
    CloseCircleOutlined,
} from '@ant-design/icons';

const Completionist = () => <span>You are good to go!</span>;

function Toefl() {
    // query 
    const { search } = useLocation();
    const navigate = useNavigate()
    const query = new URLSearchParams(search);
    const [id, setId] = useState()
    const token = sessionStorage.getItem('token');
    const [sessionFinish, setSessionFinish] = useState()
    const session = sessionStorage.getItem('session')
    const waktu = sessionStorage.getItem('waktu')
    const [tempSpace, setTempSpace] = useState([])
    const [indexSpace, setIndexSpace] = useState(0);
    const [firstWaktu, setFirstWaktu] = useState();
    const [loading, setLoading] = useState(true);
    const [longQuestion, setLongQuestion] = useState('');
    const [question, setQuestion] = useState([]);

    const [alphabet, setAlphabet] = useState(['A', 'B', 'C', 'D'])
    const [noQuestion, setNoQuestion] = useState(1);
    const [section, setSection] = useState()

    const [jawabSend, setJawabSend] = useState([]);
    const [heAnswer, setHeAnswer] = useState([]);
    const [finish, setFinish] = useState(false);

    const [pembahasan, setPembahasan] = useState([])
    const [loadSoal, setLoadSoal] = useState(false)
    const [jawabanFix, setJawabanFix] = useState([]);
    const [kunciJawaban, setKunciJawaban] = useState([])
    const [jumlahSoalKeseluruhan, setJumlahSoalKeseluruhan] = useState(0)

    const tmpJawaban = sessionStorage.getItem('jawaban');
    const [panjangSoal, setPanjangSoal] = useState([]);
    const [isiSoal, setIsiSoal] = useState([]);
    const [jumlahQuestion, setJumlahQuestion] = useState([]);

    const getLocalStorageValue = (s) => localStorage.getItem(s);

    const renderer = ({ hours, minutes, seconds, total, completed }) => {
        if (completed) {
            let answer = new FormData();
            answer.append('token', token);

            for (let i = 0; i < question.length; i++) {
                answer.append('data[' + i + '][session]', session);
                answer.append('data[' + i + '][question_id]', question[i].id);
                answer.append('data[' + i + '][he_answer]', jawabSend[indexSpace][i] == 'kosong' ? null : jawabSend[indexSpace][i]);

            }

            axios.post(`${Url}/api/v1/student/operate/answer/he-create`, answer)
                .then(res => {
                    let dataFinish = new FormData();

                    dataFinish.append('token', token);
                    dataFinish.append('session', session);
                    axios.post(`${Url}/api/v1/student/operate/counting/exam/toefl`, dataFinish)
                        .then(res => {
                            Swal.fire(
                                'Finsih!',
                                'Your exam has been Finished.',
                                'success'
                            ).then(() => {

                                let spaceOne = new FormData();
                                setIndexSpace(0);

                                spaceOne.append('session', session);
                                spaceOne.append('token', token);
                                spaceOne.append('number_space', tempSpace[0]);

                                axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, spaceOne)
                                    .then(response => {
                                        let pembahasan = response.data.data.question[0].questions
                                        setPembahasan(response.data.data.question[0].questions);
                                        console.log(pembahasan)

                                        let arrJawaban = [];
                                        let arrKunci = [];
                                        for (let x = 0; x < tempSpace.length; x++) {
                                            let tmpJawaban = [];
                                            let tmpKunci = []

                                            if (x == 0) {
                                                for (let i = 0; i < jumlahQuestion[x]; i++) {
                                                    tmpJawaban.push(pembahasan[i].he_answer[0].he_answer)
                                                    tmpKunci.push(pembahasan[i].key[0].alphabet_key)
                                                }
                                            }
                                            else {
                                                for (let i = 0; i < jumlahQuestion[x]; i++) {
                                                    tmpJawaban.push("")
                                                    tmpKunci.push("")
                                                }
                                            }

                                            arrJawaban.push(tmpJawaban);
                                            arrKunci.push(tmpKunci);
                                            // console.log()

                                        }

                                        setJawabanFix(arrJawaban);
                                        setKunciJawaban(arrKunci);
                                        console.log(arrKunci)
                                    }).then(() => {
                                        setSessionFinish(session);

                                        setFinish(true)
                                        setLoadSoal(false)

                                        sessionStorage.removeItem("waktu");
                                        sessionStorage.removeItem("session");
                                    }
                                    )

                                // })
                                // })

                            })
                        })

                })

            return <Completionist />;
        } else {
            // Render a countdown
            sessionStorage.setItem("waktu", Number(total));
            return <span>{hours}:{minutes}:{seconds}</span>;
        }
    };


    const Completionist = () => <span>Finish!</span>;

    useEffect(() => {
        getDataSpaces()
    }, [])


    const getDataSpaces = async () => {
        // mendapat info jumlah spaces 
        let formData = new FormData();
        formData.append('session', session);
        formData.append('token', token);
        await axios.post(`${Url}/api/v1/student/operate/on-going/exam`, formData)
            .then(res => {
                if (res.data.status == 'danger') {
                    Swal.fire(
                        'Failed!',
                        'Coba Pilih Paket',
                        'error'
                    ).then(() => navigate('/jenis-paket-toefl'))
                }
                let tempSpace = res.data.index_space;
                let jumlahQuestion = res.data.countQuestionBySpace;
                setId(res.data.data.info_exam.category_id)
                setTempSpace(res.data.index_space);
                setJumlahQuestion(res.data.countQuestionBySpace);

                let durasi = res.data.data.time_exam_in_milliseconds;
                let waktuMulai = Date.parse(res.data.data.info_exam.created_at)
                let batas = Number(durasi) + Number(waktuMulai)


                if (Date.now() < batas) {
                    let sisa = Number(batas) - Date.now()

                    if (!waktu) {
                        sessionStorage.setItem("waktu", sisa);
                    }
                    else {
                        sessionStorage.setItem("waktu", sisa);

                    }
                }
                else {
                    sessionStorage.removeItem("waktu");
                    sessionStorage.removeItem("session");
                    Swal.fire(
                        'Failed!',
                        'Time Out',
                        'error'
                    ).then(() => navigate('/historytest'))

                }


                // mencari soal index 1 
                let spaceOne = new FormData();
                spaceOne.append('session', session);
                spaceOne.append('token', token);
                spaceOne.append('number_space', tempSpace[indexSpace]);
                axios.post(`${Url}/api/v1/student/operate/on-going/exam`, spaceOne)
                    .then(response => {
                        let panjangQuestion = response.data.data.question.length;
                        let isiQuestion = response.data.data.question;
                        if (panjangQuestion > 0) {
                            setLongQuestion(isiQuestion[0].long_question)
                            setQuestion(isiQuestion[0].questions);
                            if (finish) {
                                setPembahasan(isiQuestion[0].questions);
                            }

                            // mengatur nama section 1
                            if (isiQuestion[0].section == 'reading') {
                                setSection('Reading')
                            }
                            else if (isiQuestion[0].section == 'listening') {
                                setSection('Listening')
                            }
                            else if (isiQuestion[0].section == 'structure') {
                                setSection('Structure')
                            }
                        }
                    })
                // mencari atau setting array jawaban
                let jawab = []
                let jwbSpc = []
                if (tmpJawaban) {

                    var jawabsave = tmpJawaban.split(',');
                    // if (tmpJawaban != '') {
                    console.log(jawabsave)
                    // console.log(tmpJawaban)

                    let totalSoal = 0
                    // mencari di session 
                    for (let i = 0; i < tempSpace.length; i++) {
                        if (i == 0) {
                            jawab = []
                            for (let x = 0; x < jumlahQuestion[i]; x++) {

                                jawab.push(jawabsave[x]);
                            }
                        }


                        else {
                            totalSoal = totalSoal + jumlahQuestion[i];
                            jawab = []
                            for (let x = 0; x < jumlahQuestion[i]; x++) {
                                console.log(totalSoal + x)
                                jawab.push(jawabsave[totalSoal + x]);
                            }
                        }
                        jwbSpc.push(jawab);
                        console.log(jawab)
                    }

                    setJawabSend(jwbSpc);

                    // }
                }


                // set jawab kosongan 
                else {
                    for (let i = 0; i < tempSpace.length; i++) {
                        jawab = []
                        for (let x = 0; x < jumlahQuestion[i]; x++) {
                            jawab.push('kosong');
                        }
                        jwbSpc.push(jawab);
                    }
                    console.log(jwbSpc)
                    setJawabSend(jwbSpc);
                }
            })
            .then(() => {
                // console.log(waktu)
                setLoading(false)

            }

            )
    }

    const getNextBackData = (index) => {
        console.log(tempSpace.length)
        let spaceOne = new FormData();
        spaceOne.append('token', token);
        spaceOne.append('number_space', tempSpace[index]);
        let section;

        if (finish) {
            spaceOne.append('session', sessionFinish);


            axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, spaceOne)
                .then(response => {
                    if (response.data.data.question.length > 0) {
                        section = response.data.data.question[0].section;
                        setLongQuestion(response.data.data.question[0].long_question)
                        let pembahasan = response.data.data.question[0].questions
                        setPembahasan(response.data.data.question[0].questions);
                        console.log(pembahasan)

                        if (section == 'reading') {
                            setSection('Reading')
                        }
                        else if (section == 'listening') {
                            setSection('Listening')
                        }
                        else if (section == 'structure') {
                            setSection('Structure')
                        }

                        let arrJawaban = [];
                        let arrKunci = [];
                        for (let x = 0; x < tempSpace.length; x++) {
                            let tmpJawaban = [];
                            let tmpKunci = []

                            if (x == index) {
                                for (let i = 0; i < jumlahQuestion[x]; i++) {
                                    tmpJawaban.push(pembahasan[i].he_answer[0].he_answer)
                                    tmpKunci.push(pembahasan[i].key[0].alphabet_key)
                                }
                            }
                            else {
                                for (let i = 0; i < jumlahQuestion[x]; i++) {
                                    tmpJawaban.push(jawabanFix[x][i])
                                    tmpKunci.push(kunciJawaban[x][i])
                                }
                            }

                            arrJawaban.push(tmpJawaban);
                            arrKunci.push(tmpKunci);
                            // console.log()

                        }

                        setJawabanFix(arrJawaban);
                        setKunciJawaban(arrKunci);
                        console.log(arrKunci)
                    }

                }).then(() => setLoadSoal(false))
        }

 
        else {

            spaceOne.append('session', session);

            axios.post(`${Url}/api/v1/student/operate/on-going/exam`, spaceOne)
                .then(response => {
                    if (response.data.data.question.length > 0) {
                        section = response.data.data.question[0].section
                        setLongQuestion(response.data.data.question[0].long_question)
                        setQuestion(response.data.data.question[0].questions);
                        if (section == 'reading') {
                            setSection('Reading')
                        }
                        else if (section == 'listening') {
                            setSection('Listening')
                        }
                        else if (section == 'structure') {
                            setSection('Structure')
                        }
                    }

                }).then(() => setLoadSoal(false))
        }


        
    }


    if (loading) {

        return (
            <div></div>
        )
    }

    function klikNext() {

        setLoadSoal(true)

        let idxSekarang = indexSpace;
        // console.log("cek" + longQuestion);
        // console.log(section)

        // menyimpan jawaban 
        if (question.length != 0) {
            let answer = new FormData();
            answer.append('token', token);



            for (let i = 0; i < question.length; i++) {
                answer.append('data[' + i + '][session]', session);
                answer.append('data[' + i + '][question_id]', question[i].id);
                answer.append('data[' + i + '][he_answer]', jawabSend[idxSekarang][i] == 'kosong' ? null : jawabSend[idxSekarang][i]);

            }

            axios.post(`${Url}/api/v1/student/operate/answer/he-create`, answer)
                .then(res => {
                    console.log(res.data);
                    getNextBackData(idxSekarang + 1);
                    setIndexSpace(idxSekarang + 1);
                })
        }
        else {
            setIndexSpace(idxSekarang + 1);
            setLoadSoal(false);
        }

    }

    function klikNextPembahasan() {

        setLoadSoal(true)
        getNextBackData(indexSpace + 1);
        setIndexSpace(indexSpace + 1);
        setLoadSoal(false)

    }


    function klikFinish() {
        setLoadSoal(true)

        let idxSekarang = indexSpace;
        let tmpsession = session;
        // menyimpan jawaban 
        if (question.length != 0) {
            let answer = new FormData();
            answer.append('token', token);

            for (let i = 0; i < question.length; i++) {
                answer.append('data[' + i + '][session]', session);
                answer.append('data[' + i + '][question_id]', question[i].id);
                answer.append('data[' + i + '][he_answer]', jawabSend[idxSekarang][i] == 'kosong' ? null : jawabSend[idxSekarang][i]);

            }

            axios.post(`${Url}/api/v1/student/operate/answer/he-create`, answer)
                .then(res => {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Finish this exam!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Finish it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            let dataFinish = new FormData();

                            dataFinish.append('token', token);
                            dataFinish.append('session', session);
                            axios.post(`${Url}/api/v1/student/operate/counting/exam/toefl`, dataFinish)
                                .then(res => {
                                    setSessionFinish(tmpsession);

                                    sessionStorage.removeItem("waktu");
                                    sessionStorage.removeItem("session");

                                    Swal.fire(
                                        'Finsih!',
                                        'Your exam has been Finished.',
                                        'success'
                                    ).then(() => {

                                        let spaceOne = new FormData();
                                        setIndexSpace(0);

                                        spaceOne.append('session', tmpsession);
                                        spaceOne.append('token', token);
                                        spaceOne.append('number_space', tempSpace[0]);

                                        axios.post(`${Url}/api/v1/admin/operate/question/answer/he`, spaceOne)
                                            .then(response => {
                                                let pembahasan = response.data.data.question[0].questions
                                                setLongQuestion(response.data.data.question[0].long_question)
                                                setPembahasan(response.data.data.question[0].questions);
                                                console.log(pembahasan)

                                                let arrJawaban = [];
                                                let arrKunci = [];
                                                for (let x = 0; x < tempSpace.length; x++) {
                                                    let tmpJawaban = [];
                                                    let tmpKunci = []

                                                    if (x == 0) {
                                                        for (let i = 0; i < jumlahQuestion[x]; i++) {
                                                            tmpJawaban.push(pembahasan[i].he_answer[0].he_answer)
                                                            tmpKunci.push(pembahasan[i].key[0].alphabet_key)
                                                        }
                                                    }
                                                    else {
                                                        for (let i = 0; i < jumlahQuestion[x]; i++) {
                                                            tmpJawaban.push("")
                                                            tmpKunci.push("")
                                                        }
                                                    }

                                                    arrJawaban.push(tmpJawaban);
                                                    arrKunci.push(tmpKunci);
                                                    // console.log()

                                                }

                                                setJawabanFix(arrJawaban);
                                                setKunciJawaban(arrKunci);
                                                console.log(arrKunci)

                                            }).then(() => {
                                                setFinish(true)
                                                setLoadSoal(false)
                                            }
                                            )

                                    })
                                })

                        }
                    })

                })
        }
        else {
            setIndexSpace(idxSekarang + 1);
            setLoadSoal(false);

        }


    }

    function klikBack() {
        // console.log(jawabanFix[indexSpace - 1])
        // console.log(longQuestion);

        let idxSekarang = indexSpace;

        setLoadSoal(true)
        getNextBackData(idxSekarang - 1);
        setIndexSpace(indexSpace - 1);

    }

    // const [value, setValue] = useState()
    function ubahJawaban(idSoal, jawab) {
        console.log(indexSpace);

        let spaceAnswer = []
        let tmpJawaban = [];
        for (let x = 0; x < tempSpace.length; x++) {
            if (x == indexSpace) {

                tmpJawaban = [];
                for (let i = 0; i < jumlahQuestion[x]; i++) {
                    if (i == idSoal) {
                        tmpJawaban.push(jawab);
                    }
                    else {
                        tmpJawaban.push(jawabSend[x][i])
                    }
                }
                spaceAnswer.push(tmpJawaban)
            }
            else {
                spaceAnswer.push(jawabSend[x]);
            }
        }
        sessionStorage.setItem("jawaban", spaceAnswer);

        setJawabSend(spaceAnswer);

        console.log(spaceAnswer)

    };

    return (
        <div className="soaltoefl">
            <div className="d-flex col flex-lg-row flex-column-reverse mt-5">
                <div className="col-xl-7 col-lg-12 me-auto ms-auto">
                    <div class="card judul-toefl mb-5">
                        <div class="card-header">
                            <div className="d-flex justify-content-between">
                                <div className="col mt-0">Toefl</div>
                                <div className="col mt-0">Section {indexSpace + 1} / {tempSpace.length} {section}</div>
                                <div className="col mt-0">
                                    {
                                        finish ? <>Finish</> :
                                            <Countdown
                                                date={Date.now() + Number(sessionStorage.getItem('waktu'))}
                                                renderer={renderer}
                                            />
                                    }


                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card soal-toefl p-4" style={{ display: finish ? 'none' : 'block' }}>
                        {
                            longQuestion != 'undefined' ?

                                section == 'Listening' ?
                                    <div className='long-question'><audio controls src={Url + "/" + ReactHTMLParser(longQuestion)} type="audio/mpeg" /></div> :
                                    section == 'Reading' ?
                                        <p class="card-text mb-2">{ReactHTMLParser(longQuestion)}</p> :
                                        section == 'Listening' ?
                                            <p class="card-text mb-2">{ReactHTMLParser(longQuestion)}</p> :
                                            null
                                : null

                        }
                        <div class="card-body" >
                            <div className="justify-content-center mb-4 mt-2" style={{ display: loadSoal ? "flex" : "none" }}>
                                <Spin tip="Loading...">
                                </Spin>
                            </div>
                            {
                                question.map((item, i) => (
                                    <div className="mb-4" style={{ display: loadSoal ? "none" : "block" }}>

                                        <div className="d-flex mb-2 mt-2">

                                            <p class="card-text me-2">{i + 1}.</p>
                                            <p class="card-text">{ReactHTMLParser(item.question)}</p>
                                        </div>


                                        {
                                            <>
                                                < Radio.Group key={indexSpace + i} defaultValue={jawabSend[indexSpace][i]} className={"pilihanke" + indexSpace + i} onChange={(e) => ubahJawaban(i, e.target.value)}  >
                                                    <Space direction="vertical" >
                                                        {
                                                            item.answer.map((answer, x) => (
                                                                <Radio value={answer.alphabet}>
                                                                    {answer.alphabet}. {answer.answer}
                                                                </Radio>
                                                            ))
                                                        }

                                                    </Space>
                                                </Radio.Group>
                                            </>
                                        }



                                    </div>


                                ))
                            }
                            <div className="d-flex justify-content-end">
                                {
                                    indexSpace == 0 ?
                                        <button type="button" class="btn btn-primary button-back-soal me-4 text-white" onClick={klikBack} disabled>Back</button> :
                                        <button type="button" class="btn btn-primary button-back-soal me-4" onClick={klikBack}>Back</button>
                                }
                                {
                                    indexSpace == tempSpace.length - 1 ?
                                        <button type="button" class="btn btn-primary button-next-soal me-4" disabled onClick={klikNext}>Next</button> :
                                        <button type="button" class="btn btn-primary button-next-soal me-4" onClick={klikNext}>Next</button>
                                }
                                {

                                    indexSpace == tempSpace.length - 1 ?

                                        <button class="btn btn-primary button-finish-soal" type="button" onClick={klikFinish}>Finish</button> :
                                        <button class="btn btn-primary button-finish-soal" type="button" disabled onClick={klikFinish}>Finish</button>
                                }



                            </div>
                        </div>
                    </div>

                    <div class="card soal-toefl p-4" style={{ display: finish ? 'block' : 'none' }}>
                        {
                            longQuestion != 'undefined' ?
                                section == 'Listening' ?
                                    <div className='long-question'><audio controls src={Url + "/" + ReactHTMLParser(longQuestion)} type="audio/mpeg" /></div> :
                                    section == 'Reading' ?
                                        <p class="card-text mb-2">{ReactHTMLParser(longQuestion)}</p> :
                                        section == 'Listening' ?
                                            <p class="card-text mb-2">{ReactHTMLParser(longQuestion)}</p> :
                                            null
                                : null

                        }



                        <div class="card-body">
                            <div className="justify-content-center mb-4 mt-2" style={{ display: loadSoal ? "flex" : "none" }}>
                                <Spin tip="Loading...">
                                </Spin>
                            </div>
                            {
                                pembahasan.map((item, i) => (
                                    <>


                                        <div className="d-flex mb-2 mt-2">

                                            <p class="card-text me-2">{i + 1}.</p>
                                            <p class="card-text">{ReactHTMLParser(item.question)}</p>
                                        </div>


                                        {

                                            <>
                                                <Radio.Group disabled key={indexSpace + i} className={"pilihanke" + indexSpace + i} defaultValue={jawabSend[indexSpace][i]} onChange={(e) => ubahJawaban(i, e.target.value)}  >
                                                    <Space direction="vertical"  >
                                                        {
                                                            item.answer.map((answer, x) => (
                                                                <Radio value={answer.alphabet}>
                                                                    {answer.alphabet}. {answer.answer}
                                                                </Radio>
                                                            ))
                                                        }

                                                    </Space>
                                                </Radio.Group>

                                                <div className="d-flex">
                                                    {

                                                        jawabanFix[indexSpace][i] != kunciJawaban[indexSpace][i] ?

                                                            <Tag style={{ verticalAlign: "none" }} className="mt-2" icon={<CloseCircleOutlined />} color="error">
                                                                Key : {kunciJawaban[indexSpace][i]}
                                                            </Tag>

                                                            :

                                                            <Tag style={{ verticalAlign: "none" }} className="mt-2" icon={<CheckCircleOutlined />} color="success">
                                                                Key: {kunciJawaban[indexSpace][i]}
                                                            </Tag>


                                                    }

                                                </div>
                                            </>
                                        }



                                    </>


                                ))
                            }
                            <div className="d-flex justify-content-end mt-5">
                                {
                                    indexSpace == 0 ?
                                        <button type="button" class="btn btn-primary button-back-soal me-4 text-white" onClick={klikBack} disabled>Back</button> :
                                        <button type="button" class="btn btn-primary button-back-soal me-4" onClick={klikBack}>Back</button>
                                }
                                {
                                    indexSpace == tempSpace.length - 1 ?
                                        <button type="button" class="btn btn-primary button-next-soal me-4" disabled onClick={klikNextPembahasan}>Next</button> :
                                        <button type="button" class="btn btn-primary button-next-soal me-4" onClick={klikNextPembahasan}>Next</button>
                                }
                                <button class="btn btn-primary button-finish-soal" type="button"
                                    onClick={() => {
                                        sessionStorage.removeItem("session"); navigate("/historytest")
                                    }}
                                >Selesai</button>

                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div >
    );

}


export default Toefl;
