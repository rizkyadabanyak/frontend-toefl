import Login from "../../login/component/Login";
import Navbar from "../../landingpage/component/Navbar";
// import "../style/Landingpage.css";

function ViewLogin() {
  return (
    <div className="viewlogin">
      <Navbar/>
      <Login/>
    </div>
  );
}

export default ViewLogin;
