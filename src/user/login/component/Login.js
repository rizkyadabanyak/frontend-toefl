import "../style/Login.css";
import bintang from "../../../asset/Bintang.png";
import { useNavigate } from 'react-router-dom';
// import {url} from "../../../../config.js";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import axios from "axios";
import Url from "../../../config"

function Login() {
    let navigate = useNavigate();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    function klikLogin() {
        let formData = new FormData();

        formData.append('email', email);

        formData.append('password', password);
        axios.post(`${Url}/api/v1/student/login`, formData)
            .then((res) => {
                console.log(res.status);
                console.log(res.data.status);
                if (res.data.status == 'success') {
                    sessionStorage.setItem("token", res.data.token);
                    navigate("/");
                }
                else {
                    Swal.fire(
                        'Gagal!',
                        res.data.message,
                        'error'
                    )
                }
            },(errror) => {
                Swal.fire(
                    'Gagal!',
                    "Error",
                    'error'
                )
            })
    }

    return (
        <div>
        <div>
            <div className="d-flex col flex-lg-row flex-column" style={{ paddingTop: "80px", minHeight: "100vh" }}>
                <div className="col-xl-6 col-lg-12 gambar">
                    {/* <div className="garis"> */}
                    <div className="row d-flex justify-content-center mt-4 mb-3">
                        <div className="garis1"></div>
                    </div>
                    <div className="row d-flex justify-content-center mb-3">
                        <div className="garis2"></div>
                    </div>
                    <div className="row d-flex justify-content-center">
                        <div className="garis3"></div>
                    </div>
                    <div><img src={bintang} className="star-image" height="40px" /></div>
                    {/* <div className="row d-flex justify-content-center mt-4 mb-0"> */}
                    <div className="garis-vertikal1"></div>
                    {/* </div> */}
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text">Selamat datang</div>
                    </div>
                    {/* <div className="row d-flex justify-content-center mt-0 mb-0"> */}
                    <div className="garis-vertikal2"></div>
                    {/* </div> */}
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text">di Lingville.</div>
                    </div>
                    {/* <div className="row d-flex justify-content-center mt-0 mb-0"> */}
                    <div className="garis-vertikal3"></div>
                    {/* </div> */}
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text-mini">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit porttitor tristique viverra enim massa ut lorem. In nunc ultrices felis lacus.</div>
                    </div>
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text-copyright">
                            © Lingville 2022.</div>
                    </div>
                </div>
                <div className=" col-xl-6 col-lg-12">

                    <div className="p-5">
                        <div className="login">Login</div>
                        <form>
                            <div class="form-email mb-3">
                                <label for="exampleInputEmail1" class="form-label d-flex flex-row" >Email</label>
                                <input type="email" placeholder="Masukkan email" class="form-control align-middle" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={e => setEmail(e.target.value)} />
                            </div>
                            <div class="form-password mb-4">
                                <label for="exampleInputPassword1" class="form-label d-flex flex-row" >Password</label>
                                <input type="password" placeholder="Masukkan password" class="form-control d-flex flex-row" id="exampleInputPassword1" onChange={e => setPassword(e.target.value)} />
                            </div>
                            <button type="button" onClick={klikLogin} class="btn button-login mb-1">Login</button>
                            <div className="register">Belum punya akun? <a   type="button" className="button-logres" onClick={() => { navigate("/register"); }} >Buat akun</a></div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}
    
    
    export default Login;
