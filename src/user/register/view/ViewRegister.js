import Register from "../component/Register";
import Navbar from "../../landingpage/component/Navbar";
// import "../style/Landingpage.css";

function ViewRegister() {
  return (
    <div className="viewregister">
      <Navbar/>
      <Register/>
    </div>
  );
}

export default ViewRegister;
