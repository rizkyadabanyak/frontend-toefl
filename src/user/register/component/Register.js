import "../style/Register.css";
import bintang from "../../../asset/Bintang.png";
import { useNavigate } from 'react-router-dom';
import { useState } from "react";
import Swal from "sweetalert2";
import Url from "../../../config"

function Register() {
    let navigate = useNavigate();

    const [nama, setNama] = useState();
    const [email, setEmail] = useState();
    const [kodeReferal, setKodeReferal] = useState();
    const [password, setPassword] = useState();
    const [konfirPassword, setKonfirPassword] = useState();

    function klikRegis() {
        if (password == konfirPassword) {
            let formData = new FormData();

            formData.append('name', nama);
            formData.append('email', email);
            formData.append('password', password);
            formData.append('referral', kodeReferal);

            fetch(`${Url}/api/v1/student/regis`, {
                method: 'POST',
                body: formData

            })
                .then(res => res.json())
                .then(data => {
                    if (data.status == 'success') {
                        console.log(data);

                        sessionStorage.setItem("token", data.data.token);
                        navigate("/");
                    }
                    else {
                        Swal.fire(
                            'Gagal!',
                            data.message,
                            'error'
                        )
                    }
                })
        }
        else {
            Swal.fire(
                'Gagal!',
                "Pastikan Password Benar",
                'error'
            )

        }


    }


    return (
        <div>
            <div className="d-flex col flex-lg-row flex-column" style={{ paddingTop: "80px", minHeight: "100vh" }}>
                <div className="col-xl-6 col-lg-12 gambar">
                    {/* <div className="garis"> */}
                    <div className="row d-flex justify-content-center mt-4 mb-3">
                        <div className="garis1"></div>
                    </div>
                    <div className="row d-flex justify-content-center mb-3">
                        <div className="garis2"></div>
                    </div>
                    <div className="row d-flex justify-content-center">
                        <div className="garis3"></div>
                    </div>
                    <div><img src={bintang} className="star-image" height="40px" /></div>
                    {/* <div className="row d-flex justify-content-center mt-4 mb-0"> */}
                    <div className="garis-vertikal1"></div>
                    {/* </div> */}
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text">Selamat datang</div>
                    </div>
                    {/* <div className="row d-flex justify-content-center mt-0 mb-0"> */}
                    <div className="garis-vertikal2"></div>
                    {/* </div> */}
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text">di Lingville.</div>
                    </div>
                    {/* <div className="row d-flex justify-content-center mt-0 mb-0"> */}
                    <div className="garis-vertikal3"></div>
                    {/* </div> */}
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text-mini">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit porttitor tristique viverra enim massa ut lorem. In nunc ultrices felis lacus.</div>
                    </div>
                    <div className="row d-flex justify-content-center mt-0 mb-0">
                        <div className="image-text-copyright">
                            © Lingville 2022.</div>
                    </div>
                </div>
                <div className=" col-xl-6 col-lg-12">
                    <div className="p-5">
                        <div className="login">Register</div>
                        <form>
                            <div class="form-email-register mb-3">
                                <label for="exampleInputEmail1" class="form-label d-flex flex-row">Email</label>
                                <input type="email" placeholder="Masukkan email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={e => setEmail(e.target.value)} />
                            </div>
                            <div className="row">
                                <div class="col form-nama mb-3 mt-0 me-3">
                                    <label for="exampleInputEmail1" class="form-label d-flex flex-row">Nama</label>
                                    <input type="text" placeholder="Masukkan nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={e => setNama(e.target.value)} />
                                </div>
                                <div class="col form-referral-code mb-3 mt-0 ms-0">
                                    <label for="exampleInputEmail1" class="form-label d-flex flex-row">Kode</label>
                                    <input type="text" placeholder="Masukkan kode registrasi" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={e => setKodeReferal(e.target.value)} />
                                </div>
                            </div>
                            <div className="row">
                                <div class="col form-password-register mb-4 mt-0 me-3">
                                    <label for="exampleInputPassword1" class="form-label d-flex flex-row">Password</label>
                                    <input type="password" placeholder="Masukkan password" class="form-control" id="exampleInputPassword1" onChange={e => setPassword(e.target.value)} />
                                </div>
                                <div class="col form-confirm-password mb-4 mt-0 ms-0">
                                    <label for="exampleInputPassword1" class="form-label d-flex flex-row">Konfirmasi Password</label>
                                    <input type="password" placeholder="Masukkan password" class="form-control" id="exampleInputPassword1" onChange={e => setKonfirPassword(e.target.value)} />
                                </div>
                            </div>
                            <button type="button" class="btn button-login mb-1" onClick={() => { klikRegis(); }}>Buat Akun</button>
                            <div className="register">Sudah punya akun? <a type="button" className=" button-logres" onClick={() => { navigate("/login"); }}>Login</a></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;
